# Development

## Local Development

> This is the information needed to develop this site locally. 

### Commands

All commands are run from the root of the project, from a terminal:

| Command               | Action                                             |
| :-------------------- | :------------------------------------------------- |
| `npm install`         | Installs dependencies                              |
| `npm run dev`         | Starts local dev server at `localhost:3000`        |
| `npm run build`       | Build your production site to `./dist/`            |
| `npm run preview`     | Preview your build locally, before deploying       |
| `npm run format`      | Format codes with Prettier                         |
| `npm run lint:eslint` | Run Eslint                                         |
| `npm run astro ...`   | Run CLI commands like `astro add`, `astro preview` |

When you like how things look during `npm run dev`, run `npm run build` to generate the `dist` directory that Firebase will look to distribute.

## Staging

Push to a preview channel on Firebase:

```
firebase hosting:channel:deploy new-awesome-feature --expires 1d
```

This preview channel will be called `new-awesome-feature` and it will automatically expire in one day. The exact date and time of expiration are included in the output of the command above as well as URL for the preview channel. 

Read [the docs](https://firebase.google.com/docs/hosting/manage-hosting-resources#preview-channel-expiration) for more information. 

## Push to Production

The site it hosted by Firebase. With the right Firebase creds, run `npm run build` to build the site and then `firebase deploy` to push straight to prod.
