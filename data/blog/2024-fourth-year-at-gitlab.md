---
title: "4️⃣ Four Things about my Fourth Year at GitLab"
description: "🚀 A difficult but rewarding year."
excerpt: "🚀 A difficult but rewarding year."
highlight: "🚀 A difficult but rewarding year."
slug: four-years-gitlab
publishDate: 2024-05-27 22:20:31
tags: ['support','engineer','gitlab','reflection','annual','tradition','work']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/merge-conflicts.jpg
fullscreen: true
---

> The photograph above was taken in Thomas, West Virginia.

🌊 Hiya and welcome to my fourth annual reflection on the previous twelve months of work at GitLab. The last year was one of the most difficult but rewarding years of my professional career. 🪞 In this year's reflection post, I'm going to share four miscellaneous ✨ things ✨ that about the last twelve months:

# 1️⃣ An Exciting Thing

🤭 If you are a frequent reader, you can probably predict what the exciting thing is: my promotion to `Staff Support Engineer` became effective this year! Between the [blog post](/journey-staff) and the [video](https://youtu.be/50LObql1dx8?si=Z3MuegKAUA2Zom7W), I've said quite a bit about that already so let's move on.

# 2️⃣ A Heartwarming Thing

I had opportunities to meet folks I work with in-person this year. Joining GitLab shortly after the pandemic began meant it was quite a long time before I met any of my teammates in-person. (I checked my "🤳 Selfies with GitLab people!" album and can tell you that July 2022 was the first time I met someone from GitLab in-person.)  It was _beyond_ exciting to make the Zoom-to-IRL transition with so many folks.

At the same time, I am struck by how little being all-remote matters! By that, I mean: it is absolutely possible to establish deep, meaningful, long-lasting relationships when totally remote. It's one thing to know the truth of that in theory and another thing for it to be lived experience.

# 3️⃣ A Worthwhile Thing

I've long been a fan of using Google Forms to track various things over time. By [saving my responses in a Google Sheet](https://support.google.com/docs/answer/2917686?hl=en#zippy=%2Cchoose-where-to-store-responses), I can get the data as a `.csv` file and do (almost) whatever I want with it!

This year, I got a lot of benefit out of tracking things in two specific areas. The first one I'll share is new this year but the second has been useful to me for a few years now.

Part way through this last year, I started keeping track of how I spent my time each day. I'd do this by choosing from a few categories of things.

 In the "🐈‍⬛ Big picture: where did your time go?" form, the responses for an example week could look like this:

  - Monday: **On-Call**, **Meetings**
  - Tuesday: **Tickets**, **MRs**
  - Wednesday: **1:1s**, **Meetings**
  - Thursday: **1:1s**, **Pairing**, **Team Process Improvements**
  - Friday: **Pairing**, **Tickets**, **Focus Mode**

The idea here is not to keep track of everything that I did on a given day. The list of categories isn't quite right yet. While this form was totally worthwhile, I expect to iterate on it in the year ahead and get more value from it in the future.

The other form is more personal: in the "🌞 How are you, Brie?" form, I track answers to questions like:

- On a scale from 1-10, how **happy** are you?
- On a scale from 1-10, how **stressed** are you?
- In an emoji, how would you describe the day?
- In an emoji, how would you describe the week?

I always leave an open text field so I can write a few notes about what was going on at the time I replied. Over time, I learned what my baseline responses were for the first two questions. I discussed trends in my responses with my manager. That permitted me to say things like "I'm back from 🏝 OOO and I _know_ it helped because my happy and stressed numbers are looking much better now".

# 4️⃣ An Excellent Thing

🏆 I [was recognized](https://handbook.gitlab.com/handbook/engineering/recognition/#fy24-q4) as an [Engineering Quarterly Achiever](https://handbook.gitlab.com/handbook/engineering/recognition/#engineering-quarterly-achievers), earning the **Performance and Scalability Award** in the [Customer Results](https://handbook.gitlab.com/handbook/engineering/recognition/#customer-results) category.

# 💖 From the Heart

It has been five years since I finished grad school. As I was pursuing my Master's degree, I was very open-minded about how I thought about how I would answer the onslaught of "what will you do with your degree?" inquiries. (That's another way of saying "I was not sure what I wanted to _do_ with my degree".) Studying cybersecurity gave me an exceptionally broad set of options. 🦊 Growing into a leadership role on the Support team at GitLab has been a really exciting answer to the inquiry and ✨ I remain excited beyond measure to see what the future has in store in the year ahead.

✌️