---
title: "April 2024 - My Journey to Staff Support Engineer"
highlight: "How I became a Staff Support Engineer."
description: "How I became a Staff Support Engineer."
excerpt: "🗺 A timeline of my path to Staff Support Engineer, a list of things I'm glad I did and a few tips for folks considering a similar journey."
slug: journey-staff
publishDate: 2024-04-11 23:55:31
tags: ['support','engineer','promotion','reflection','advice','staff','work']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/dontlimityourselfie.jpg
fullscreen: true
---

> ☝️ A wall I saw in Denver in 2022 and a very good reminder.

Hello, world! 

To help you determine whether this post is of interest to you, here's a brief outline of what I will do:

- share a brief timeline of my path to becoming a Staff Support Engineer at GitLab
- discuss a few things I'm very glad I did along the way
- provide a few tips for others considering a similar journey
- share my approach to "omg I've been promoted; now what?" for folks new to _any_ role

I have been super excited about this blog post for quite a while so let's dive right in! 

## 📆 Brief Timeline

For a bit of context, here's a brief timeline of my path to the `Staff` Support Engineer role at 🦊💜 GitLab.

- May 2020 | I joined the GitLab Support team as an `Intermediate` Support Engineer.
- August 2022 | My promotion to `Senior` Support Engineer became effective.
- January 2023 | [The Staff Engineer’s Path](https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/) leadership book club began. Looking back, I now realize that this was the first serious step on my journey to `Staff`.
- April 2023 | I wrote [A Staff (Support) Engineer's Path](https://brie.dev/staff-engineer-path/) to summarize what I gleaned from the conversation and thought fostered by the book club.
- May 2023 | I created my [promotion document](https://handbook.gitlab.com/handbook/support/workflows/team/promotions/#promotion-document), marking the formal beginning of the process.
- December 2023 | I submitted my promotion document.
- March 2024 | My promotion to `Staff` Support Engineer was announced. 

The book club did a lot to help focus my thinking. During 2023, I feel like I "became" a Staff Support Engineer. I started to take on more strategic work. I worked to split my time between my own contributions and mentoring others. When opportunity knocked, I was ready and began to work cross-functionally to the benefit of the GitLab Support team and the broader organization. 

## ✨ Specific Things I am Glad I Did

Advice is just something that worked well for someone else. This is what worked for me:

- 📓 **Journal** periodically.
- 🛤 **Track my work**.
- 💡 Have an **elevator pitch** for my ideas and efforts.
- 🤝 Remember that **together is better**. 
  - Watch Manu and I discuss [our paths to promotion](https://youtu.be/50LObql1dx8?si=Z3MuegKAUA2Zom7W) and the value of having a promotion buddy.
- 📸 Think big picture: improve the approach to entire **types** of tickets. Don't just be satisfied with solving a single occurrence of the problem.
- ☕️ Set up [coffee chats](https://handbook.gitlab.com/handbook/company/culture/all-remote/informal-communication/#coffee-chats).
- 🐾 Iterate with intention.

While these things served me well during the promotion process, they are also ongoing practices. I have mentioned drawing inspiration from Cynthia Ng previously and I recommend her notes on [tracking one's work for performance reviews and promotions](https://cynthiang.ca/2023/01/09/tracking-your-work-for-performance-reviews-and-promotions/) in particular. 

## 👋 Tips on Becoming Staff

- 🍵 Expect to be "T" shaped, as [other Staff Support Engineers at GitLab discuss](https://youtu.be/68jzHfG8SwU?si=gHhfbYLwpHQuwQiY&t=2197) in a May 2022 Group AMA on the role. 
  - Be helpful broadly. That includes knowing who the SMEs are and pointing folks in the right direction. Don't try to know it all or do all the things yourself.
- ❤️ The _full_ extent of your impact will not be known to you.
  - Working to [influence at scale](https://staffeng.carranza.engineer/BookNotes/staff-engineer-path_chapter08/) means you'll have a direct and positive impact on someone with whom you are not familiar.
- 🦄 Staff Support Engineers are like unicorn snowflakes. No two Staff Support Engineers are alike and that's a good thing. That can also be a very stressful and isolating thing. 
  - Remember that comparison is the thief of joy; be at peace with your own unique way of contributing to the team. 
- 🍀 Opportunity knocks but you won't know when or what it looks like. Have a plan (so you know what you're working on) but be flexible and ready to pivot when you hear the knock.

## 🙋 I've been promoted: now what?

> The move to `Staff` is on my mind but this section applies to newly promoted folks in a variety of roles. 

I do not have all of the answers to this question. I do have a few thoughts based on how I have navigated being promoted into new and exciting roles during my years working in tech. Based on that general experience and the situation in front of me today, here's how  I plan to address the question above:

- 🫁 Breathe. You have some time to get your bearing.
- ✌️ Keep doing what you have been doing. 
  - Why? That’s what got you promoted in the first place.
- 🥅 Set specific reasonable goals for your first `X` days in the role. 
- 💼 Chat with your manager. 
  - Understand where they think you should focus. 
  - Understand the problems _they_ face and how you fit into managing or solving those problems.
- ℹ️ Use the job description to guide you.
  - Even if it's not perfect or comprehensive, focus on the core responsibilities and expected outcomes.
- 🤝 **Help the team**.
  - Unblock. (That can sometimes be as simple as "I would suggest checking in with `soandso`".)
  - Think about solving **types** of problems that the team faces systemically (instead of just solving that problem once for a particular customer).    

💪 We can do this! I'll be seeking to follow the path I've laid out above over the next few months. Let me know how things go for you in your new role: 📯 [brie@infosec.exchange](https://infosec.exchange/@brie).

## 💖 From the Heart

A year ago, I wrote [A Staff (Support) Engineer's Path](https://brie.dev/staff-engineer-path/) shortly before creating my promotion document. If I could go back in time to that version of myself, these are a few kind, gentle things I would say to myself:

Ease your mind: during the process, it will feel like a difficult journey and a long, long road. In the end, "this too shall pass" is true of all things. Even when it feels like it, know that you are not totally alone. There are, however, some things you must bear and some decisions you must make alone. 

- Plan your work; work your plan. 
- Embrace uncertainty; expect change.

🦁 You are **far** more ready than you think. Be bold: leap.

## 🌌 What's next?

In the time since I first started drafting this blog post, opportunity knocked in a very interesting and exciting way that I had not anticipated. As I wondered aloud on the `/about` [page](https://brie.dev/about) of this very site: **who knows what the future holds?**  

🔮 Only time will tell.

 --
 Brie 🌈 🦄
