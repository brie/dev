---
title: "🪞 2023, a year"
description: "🪞 Reflections on my goals and accomplishments for the year."
excerpt: "🪞 Reflections on my goals and accomplishments for the year."
highlight: "🔭 2023 in review"
slug: "2023"
publishDate: 2023-12-31 00:00:21-05:00
tags: ['2023','reflection','year','review','mastodon','toot','accountability', 'goals']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/curious-cat.png
fullscreen: true
created: 2023-12-30T01:22:34-05:00
modified: 2023-12-31T00:00:21-05:00
---

🌊 Hello, world!

In March, I 📯 [tooted](https://infosec.exchange/@brie/110110019259390864): 

> If you want to have a cool "my 2023 year in review" post, now is a fantastic time to get on it. You have the first quarter of the year to reflect on and nine months of planning and execution left to do. 🚀

Over the course of the year, I:

- kept track of some of the things I did in a private Google Doc. 
- continued my experimentation with various forms of public goal setting.

# 🎊 Excerpts from my "✨ 2023 Year in Review 🦄" doc

- 🚀 I [published](https://brie.dev/2023-september-pastebin-bisque-v1/) my `pastebin-bisque` project to [PyPI](https://pypi.org/project/pastebin-bisque/) from a rooftop in Amsterdam on my 🎂 birthday.
- ✍️ I wrote a few things this year that I am particularly excited about:
  - the [written](https://brie.dev/troubleshooting) version of my **Troubleshooting like Batman** talk
    - 🎥 Watch the [talk](https://vimeo.com/795905655)
  - [Using Markdown for Mindmaps with Markmap](https://brie.dev/2023-markdown-mindmap/)
  - my post celebrating [3️⃣ three years at GitLab](https://brie.dev/three-years-at-gitlab/)
- 🏆 I completed the half marathon at the Pittsburgh Marathon and the entirety of the 10K of Hell Hath No Hurry.
- 🌻 I had a successful flower garden. In addition to a healthy batch of sunflowers, I branched out and grew dwarf dahlias and nasturtiums this year.
- 🐈 I published a quick early version of a [utility](https://github.com/bbbbbrie/grontojq) in Go that solves a specific  problem that I had: turning the output of `gron` into `jq` commands.

## 📼 LIKE AND SUBSCRIBE

I am _extremely_ excited about the videos that went on YouTube this year. I got to interview a few of my colleagues:

- [Bruno Freitas Interviews Brie Carranza | GitLab Support](https://www.youtube.com/watch?v=7hUIyTX_0Ow)
- [Brie Carranza Interviews Manuel Grabowski | GitLab Support](https://www.youtube.com/watch?v=OxR_GTTawLE) | Bonus: watch [recordings](https://gitlab.com/manuelgrabowski/team-tracking/-/issues/27#recordings) from a super exciting programming project we worked on.

Plus, you can watch me [install and use](https://youtu.be/uOGNnG_qPNo) `pastebin-bisque` in under a minute.

# 🌐 Progress on Public Goals

- ✅ I [decided to try](https://brie.dev/2023-january-articles-notes-commands/) blogging once per month in 2023. This is the tenth post of the year. I took a break from blogging while working on my garden: I am _very_ happy with my progress.
- ✅ After publishing `pastebin-bisque`, I wanted to keep the momentum up by [setting out](https://infosec.exchange/@brie/111169018152966991) to 🚀 ship some wishlist improvements. I [did it](https://infosec.exchange/@brie/111173565918067779) the next day. That wasn't a super ambitious goal, it seems. That's OK. Everything doesn't have to be difficult.

## 🎁 2023 Wrapped

I visited a few cool countries this year: **France**, **Germany**, **Mexico** and  **The Netherlands**. I spent time in a few neat cities in the US: **Chicago**, **Philadelphia** and **Phoenix**. I took a few lovely 🚂 train rides through **Europe** and 🛫 I flew **19,237** miles in 2023.

🎧 Hands down, my top musical artists were **Gojira** and **Mastodon**. I 🎸 went to see them on the **Mega Monsters** tour in August.

![](/posts/2023wrapped_summary-share.jpeg)

🎬 The best movie I [watched](https://trakt.tv/users/brie/year/2023) this year was **Baby Driver**. 

# 🐈‍⬛ Motivation

I was really torn on whether publishing this as a blog post made sense. I already have the Google doc and the publicly set goals. What's the point of writing all of this up?

The Google Doc was just a list of items without even these brief notes about how excited these accomplishments made me feel. I wanted to capture that information in order to have it and because I always appreciate looking back at things I've written. Being able to link back to this kind of stuff has been super helpful this year. Working in public raises the bar for the quality of what I write but it does make deep reflection a bit more difficult. I'm happy with this tradeoff because I do like being able to easily share this kind of information with others. I've also heard often from folks who have read and enjoyed my blog posts. I've always hoped that others would find my writing helpful. Knowing that it does makes me happy so I won't stop. Lastly, being more vulnerable in the open has made me a stronger and better person.

Goal setting can be tricky. Well-constructed goals can be motivational and help with accountability. It's easy for goals to become traps. Be honest and kind with yourself. Stick to your goals to the extent that they serve you. Be free to outgrow your old goals and embrace exciting new ones.

💖 Be well.
