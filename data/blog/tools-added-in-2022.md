---
title: "Tools I added to my toolbox in 2022 🧰 "
description: "Here's how I did it."
slug: 2022-tools-added-to-toolbox
publishDate: 2022-12-31 14:38:21
tags: ['datasette','cloud','json','hurl','ci']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/2022-sunflower.jpeg
excerpt: "A quick description of the coolest tools I added to my toolbox in 2022 and links to what I built with some of the tools."
fullscreen: true
---


I really like trying out different tools and thinking about creative ways to use them to solve problems. I test out many things that go nowhere. In this post, I capture a few of the coolest tools I added to my tech toolbox in 2022. 

The tools I grew to love in 2022 are:

  - Datasette
    - Datasette is an open source multi-tool for exploring and publishing data. Learn more at [datasette.io](https://datasette.io/) or on [Simon Willison's blog](https://simonwillison.net/).
    - This year, I used Datasette to [create a heatmap](https://412.brie.run/steps/steps) of Pittsburgh's city steps. Thank you to the [Western Pennsylvania Regional Data Center](http://www.wprdc.org/) for a beautiful dataset.
    - This year, I built a [site to collect TIL ("today I learned") snippets of cool things I learned](https://til.brie.dev/) with Datasette.
    - This year, I used Datasette for a Web interface to `ripgrep` searches on the Enron dataset. I wrote this [TIL snippet](https://til.brie.dev/datasette/webui-for-searching-code-with-datasette-and-ripgrep) about the setup.
    - Datasette is useful for exploring, analyzing, visualizing and publishing all kinds of data. Datasette works best with an SQLite database. Tools abound for converting CSVs to SQLite. 
  - Google Cloud Run
    - Google Cloud Run is a fully managed compute platform that automatically scales containers. Learn more at [cloud.google.com/run](https://cloud.google.com/run).
    - This year, I deployed the [HTTP Status Cats](https://httpcat.us) site to Google Cloud Run. A project that's still in closed alpha will be deployed there as well. Google Cloud Run is fantastic for Flask apps. Flask is a favorite tool of mine but it's not new in 2022. 
  - `gron`
    - The `gron` utility transforms JSON into discrete assignments to make it easier to `grep` for what you want and see the absolute 'path' to it.
    - I find `gron` super useful for _quickly_ getting to what I want in a JSON file. For me, it's much quicker than `jq`. 
  - `hurl`
    - The `hurl` utility is a command line tool that runs HTTP requests defined in a simple plain text format. Learn more at [hurl.dev](https://hurl.dev/).
    - 🚀 Get started today with my [sample `hurl` project](https://gitlab.com/brie/hurl). 
    - This year, I wrote the [Automate HTTP Testing with hurl: Generate HTML and JUnit reports via GitLab CI](https://brie.dev/2022-http-testing-hurl/) blog post about my experience getting started with `hurl`. 
  - `jless`
    - The `jless` tool  is a command-line JSON viewer designed for reading, exploring, and searching through JSON data.
  - jqplay
    - The jqplay site is a playground for jq 1.6. Give it a go at [jqplay.org](https://jqplay.org). 
    - I find `jqplay` useful for testing `jq` filters on non-sensitive JSON data. The ability to create a permalink and share snippets is a very nice feature. ✨
  - MkDocs
    - MkDocs is a fast, simple and downright gorgeous static site generator that's geared towards building project documentation. Learn more at [mkdocs.org](https://www.mkdocs.org/).
     - This year, I deployed [🔖 bookmarks.brie.dev](https://bookmarks.brie.dev/) using MkDocs to deploy my Markdown based notes to static HTML deployed with GitLab Pages. Fork [the starter project](https://gitlab.com/gitlab-org/frontend/playground/obsidian-and-gitlab-pages-demo) that I forked to get your own site. 
  - r2devops
    - r2devops.io is a collaborative hub of CI & CD jobs which helps you to quickly build powerful pipelines for your projects.
    - In 2022, I started integrating some of these templates into my personal projects. 
  - Tailwind and friends!
    - Tailwind CSS is a utility-first CSS framework packed with classes like flex, pt-4, text-center and rotate-90 that can be composed to build any design, directly in your markup. Learn more at [tailwindcss.com](https://tailwindcss.com/).
    - Tailwind is great but the things that you can build on top of it are _amazing_! I especially recommend these **friends** of Tailwind:
      - [Flowbite](https://flowbite.com/) is a free and open-source library of over 450+ UI components, sections, and pages built with the utility classes from Tailwind CSS and designed in Figma.
      - [Tailwind Color Palette](https://tailwindcolor.com/) is a click-to-copy Tailwind color palette. Click a color and get the corresponding hex, RGB, HSL or Tailwind class.
      - [Wicked Blocks](https://wickedblocks.dev/) is a free collection of over 120 fully responsive Tailwind blocks & components you can copy paste into your Tailwind projects. 


Be well! 👋

---

The image atop this post is a photograph that I took of a sunflower that I grew in 2022. I have more sunflower photos available in [HELIANTHUS](https://sunflower.gallery/), my sunflower photo gallery at `sunflower.gallery`.