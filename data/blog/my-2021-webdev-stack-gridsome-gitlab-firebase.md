---
title: "My 2021 Web Development Stack: Gridsome + Firebase + GitLab = <3"
description: "Here's how I did it."
slug: 2021-webdev-stack
publishDate: 2021-03-13 16:44:21
tags: ['web','markdown','javascript','ssg','gitlab','ci','cd','firebase']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/boombox.jpg
excerpt: "Modern Stack for fast web development"
fullscreen: true
---

During my undergraduate studies in the late aughts, I did a lot of Web development. I learned mostly JavaScript, SVG and CSS. Dr. David Dailey at SRU was absolutely incredible at demonstrating that an entire universe of possibility awaited the patient and curious. 

  - Spend [an afternoon](http://srufaculty.sru.edu/david.dailey/) taking a look


Fast forward a decade or so and I've been back to Web development in earnest from 2020 and into 2021 thus far. I have found the following to be a quite quick, flexible and beautiful way of going from "cool idea" to "working website". I'm starting to make a dent on standing up websites for all of the domain names I've bought. (I've stopped purchasing new ones. Could I have a website for every domain name I own by the end of 2021?):

  - Static site generator (Hugo or Gridsome) to build the site, they support themes and starters so you can have a pretty site while you dust off the cobwebs of your CSS knowledge
  - Firebase for Web hosting (HTTPS, perfect for static sites, preview channels, free for most use cases)
  - GitLab CI to pull it all together: generate the site, deploy to a preview Firebase channel and manual push to production)


## Static site generators (SSGs)

I tend to start with [Hugo](https://gohugo.io) or [Gridsome](https://gridsome.org). I also think that [Expose](https://github.com/Jack000/Expose) creates beautiful photo gallery websites. 
If you are impatient and want to jump right in, I recommend choosing [a theme](https://jamstackthemes.dev/) from `jamstackthemes.dev` that appeals to you and using that to guide your choice of SSG. I wound up becoming proficient with a few SSGs  before choosing one to focus on.

There are [a ton](https://staticsitegenerators.net/) of static site generators but I would recommend taking a look at `jamstack.org` and [their list](https://jamstack.org/generators/) for good ones to start with. 


## Firebase

Firebase is [a Google service](https://firebase.google.com/) that does a bunch of different really useful things. I take advantage of [Firebase Hosting](https://firebase.google.com/docs/hosting) for most of my sites. Firebase supports custom domains with digital certificates on a free tier that's met all of my needs. And [deploying to Firebase via CI](https://about.gitlab.com/blog/2020/03/16/gitlab-ci-cd-with-firebase/) is a breeze. 


> Firebase + GitLab CI = <3



## GitLab CI/CD

I deploy to Firebase via GitLab CI. Here's the `.gitlab-ci.yml` that I use to deploy this site, [brie.dev](https://brie.dev):

```
Gridsome Build:
  stage: build
  image: node:12.13.0-alpine
  before_script:
    - npm install -g @gridsome/cli
    - cd gridsome && npm install
  script:
    - gridsome build
    - gridsome info
  artifacts:
    paths:
      - gridsome/dist/

Deploy to Firebase:
  stage: deploy
  image: node:12.13.0-alpine
  needs:
    - "Gridsome Build"
  before_script:
    - npm i -g firebase-tools
  script:
    - firebase deploy --token $FIREBASE_TOKEN --project briedev
```

The first job is called **Gridsome Build**. After installing the CLI for Gridsome, the `gridsome build` command is run to convert the Markdown and Vue into static HTML. That HTML is saved in `gridsome/dist` and passed as an artifact to the second job. The second job, **Deploy to Firebase**, installs the tools needed to deploy to Firebase and deploys the contents of `gridsome/dist` to the Firebase project called `briedev`. The [firebase.json](https://gitlab.com/brie/dev/-/blob/main/firebase.json) file is what tells the `firebase` CLI that the files we want are in `gridsome/dist`. Firebase takes care of making the HTML available over the web via HTTPS at `https://brie.dev`. 


## Real-World Examples

Here are a few sites that I am deploying with the method described above today:

  - [brie.dev](https://brie.dev) - this very site! | the [.gitlab-ci.yml](https://gitlab.com/brie/dev/-/blob/main/.gitlab-ci.yml)
  - [icanhazk8s.com](https://icanhazk8s.com)
  - [sunflower.gallery](https://sunflower.gallery) - I'm using [Expose](https://github.com/Jack000/Expose), a beautiful static site generator for photo galleries. | [source](https://gitlab.com/brie/sunflower-house-calendar)







