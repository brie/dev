---
title: "A look back at one year at GitLab"
highlight: "A look back at one year at GitLab"
description: "Here's how I did it."
slug: year-at-gitlab
publishDate: 2021-05-26 21:11:32
tags: ['work','reflections','collaboration','values']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/hat-socks-stickers.png
excerpt: "Thoughts after a year on the GitLab Support team."
fullscreen: false
---

Earlier this year you may have read the one year work anniversary posts by my fellow GitLab team members:

  - March 2021: [My 1st year all-remote at GitLab](https://dnsmichi.at/2021/03/02/my-1st-year-all-remote-at-gitlab/) - Michael Friedrich
  - April 2021: [One year at GitLab: 7 things which I didn't expect to learn](https://svij.org/blog/2021/04/12/one-year-at-gitlab-7-things-which-i-didnt-expect-to-learn/) - Sujeevan Vijayakumaran

Following in the footsteps of these friends, I wanted to share my reflections on my experience at GitLab. I started at GitLab on **May 26, 2020**, a few days after [GitLab 13.0 was released](https://about.gitlab.com/releases/2020/05/22/gitlab-13-0-released/). I became aware of [the GitLab values](https://about.gitlab.com/handbook/values/) as I considered choosing GitLab. After a year watching the values in action, the values provided an obvious template for organizing my thoughts.

--- 

## 🤝 Collaboration

**TL;DR**: OUT OF THIS WORLD. I AM ABSOLUTELY BLOWN AWAY. async collab = <3

The GitLab Support team is global and comprised of absolutely incredibly talented engineers who excel at identifying nuanced problems in large environments built and maintained by others. Re-read that sentence. That's an incredible ask but we do it **together** all day, every day. 

### async collab = <3
It is not uncommon for me to work with a colleague on the other side of the Earth. Async collaboration empowers this. I really love [pairing](https://gitlab-com.gitlab.io/support/support-pairing/) with my colleagues. The synchronous approach works quite well but is not the best for all engineers or for all situations. One of my favorite things to do is to take a problem off into a corner, poke at it for a while and assemble my findings into a set of notes for a colleague to review. The freedom to take my time, test different theories and be thorough leads to some of my best work and most interesting findings. 

...and that's just the Support team. We work with folks from all across the organization who assist in our efforts to field customer requests. When I have a question or things are not quite working properly, I get to ask the person who wrote the code. It feels so natural and obvious to me now but anyone who has done tech support surely sees the power of this possibility. 

...and that's just GitLab team members. The collaboration knows no limit where [everyone can contribute](https://everyonecancontribute.com/). So many [community resources](https://about.gitlab.com/community/)!


The [kindness](https://about.gitlab.com/handbook/values/#kindness) and [power of positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent) here are beautiful and resonate deeply with my approach to life. Bringing these to work and pairing them with a culture that understands that [it's impossible to know everything](https://about.gitlab.com/handbook/values/#its-impossible-to-know-everything) gives me everything I need to dive into the unknown, knowing that [the team wants to see me succeed](https://about.gitlab.com/handbook/values/#see-others-succeed) and [won't let me fail](https://about.gitlab.com/handbook/values/#dont-let-each-other-fail). 


## 📈 Results

Simple but powerful. I choose how I get my work done. What matters is bringing a problem to an effective resolution. The requirements are very clear. As I [once twote](https://twitter.com/whoamibrie/status/1327086726985211906?s=20):

https://twitter.com/whoamibrie/status/1327086726985211906

The results-oriented model is not at the expense of our humanity. One of our subvalues is: [Family and friends first, work second](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second). Check #FamilyAndFriends1st to observe our monthly **Family and Friends Days** as one of the many ways that this subvalue is practiced out loud and often. 

## ⏱️  Efficiency

I naturally embrace this value. I think of [efficiency](https://about.gitlab.com/handbook/values/#efficiency) as a close friend to iteration. What's the smallest thing we can do to effect change? It's not about refusing to think big. It's about looking for value in impact when balanced with effort and time. 

With GitLab's approach to transparency, practicing  [self-service and self-learning](https://about.gitlab.com/handbook/values/#self-service-and-self-learning) ensures that I have access to what I need to do my job. Being able to review conversations about **why** we decided to go in a certain direction provides extremely useful context when fielding requests and asembling bug reports or feature proposals. 

**Are you busy at work? Yes, so, be efficient where you can.**

**Favorite Subvalue**: [boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions)

## 🌐 Diversity, Inclusion and Belonging

_I joined GitLab in May 2020, the day after George Floyd was murdered. For this and a great many other reasons, this value has never been far from my mind. I would need quite a bit of time and space to compose my thoughts on this topic so I'll be brief and limit my scope for the purposes of this post._ 


 A year later, I am quite pleased with how serious and intentional the inclusivity and approach to [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) are at GitLab. To more clearly understand this, I would encourage you to:

   - Read through the [Diversity, Inclusion & Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) section in the handbook
   - Know that GitLab lives the values out loud

Together with Morehouse College, GitLab provided a [course on Advanced Software Engineering](https://about.gitlab.com/company/culture/inclusion/erg-minorities-in-tech/advanced-software-engineering-course/). I was quite excited to be interviewed sharing parts of my career story in a conversation with [Sharif Bennett](https://about.gitlab.com/company/team/#SharifATL): [watch it on YouTube](https://youtu.be/d2vo_--lX-M).

https://youtu.be/d2vo_--lX-M

Belonging is a really interesting word. I previously thought about acceptance in the workplace. But acceptance sounds like tolerance. Do you want to be tolerated or do you want to *belong*? I feel that I *belong* at GitLab. This is made clear to me in a variety of ways. Perhaps the most exciting....there's a `:brie:` Slack emoji! It is extremely clear to me that I belong at GitLab. I savor that certainty. 

## 👣 Iteration

This is the toughest but the most rewarding. Embracing iteration means [proceeding deliberately](https://about.gitlab.com/handbook/values/#always-iterate-deliberately) balanced with ["don't wait"](https://about.gitlab.com/handbook/values/#dont-wait).

 I was not initially compelled by the topic of iteration but I fully embrace it my approach to my work and my personal development efforts and -- I. SHIP. MORE. I highly recommend scrolling through the [Iteration](https://about.gitlab.com/handbook/values/#iteration) subvalues and consider what it would look like to apply them in your individual endeavors and with your team. This Forbes article provides a great look at [the relationship between creativity and iteration](https://www.forbes.com/sites/adigaskell/2021/04/27/creativity-is-a-persistent-endeavor/?sh=1940ebbe5061).

## 👁️ Transparency

This was the biggest adjustment. It was an adjustment because of how seriously GitLab takes [transparency](https://about.gitlab.com/handbook/values/#transparency). The simplest way to note this is to point to our [public by default](https://about.gitlab.com/handbook/values/#public-by-default) subvalue. If they aren't already doing so, imagine for a moment what it would look like if your current organization really implemented **public by default**. What impact would that have on your ability to get things done? 

Even this post is an example of how I have adjusted to Transparency over the course of the last year. A year among people who freely share [inspiring](https://boleary.dev/blog/2020-12-04-understanding-what-we-dont-understand.html), [informative](https://cynthiang.ca/2020/12/11/prioritization-in-support-tickets-slack-issues-and-more/) and [introspective](https://rambleon.org/2021/04/22/my-little-symphony/) blog posts on the Internet and heartfelt moments in [places like #mental_health_aware](https://about.gitlab.com/company/culture/all-remote/mental-health/mental-health-awareness-learning-path/#id-like-to-learn-more-from-my-gitlab-team--where-can-i-go-to-talk-about-my-mental-health) has freed me to explore a much more introspective post than I ever would have shared previously. (Publicly, I kept strictly to the technical in my writing. Even on this very site -- until [the "DIY Cat Cave" post](https://brie.dev/diy-cat-cave/).)


**[Transparency is only a value if you do it when it is hard](https://about.gitlab.com/handbook/values/#transparency-is-only-a-value-if-you-do-it-when-it-is-hard)**

This is a subvalue I've reflected on a lot and I extend the sentiment to most values, in life not just the GitLab values. 

---

Above all, I am so thankful for the goodness and kindness of the people I have met at GitLab. There are far far too many names to enumerate but please know that I appreciate every single Slack mention, #thanks post, issue comment, MR, pairing session and [coffee chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats).

--

Brie 
