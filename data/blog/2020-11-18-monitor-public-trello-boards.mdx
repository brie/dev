---
title: "Monitor Public Trello Boards"
description: "A CI pipeline template using the Trello API to monitor the list of public Trello boards for users of interest."
slug: 2020-11-monitor-public-trello-boards
publishDate: 2020-11-18 16:44:21
tags: ['osint', 'trello', 'api','gitlab','ci', 'pipeline']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/pothos-hawaii.jpg
excerpt: "Monitor your Trello footprint before it matters."
---

Trello is an awesome producitvity tool that is especially great for real-time and asynchronous collaboration. Trello boards can be set to **Public** and are then available on the Internet. 

A while back, I wrote a Python utility called [trello-public-board-lister](https://github.com/bbbbbrie/trello-public-board-lister) that used the Trello API to return a list of all of the **Public** boards owned by a Trello user that I specified. I picked it back up recently and extended the functionality:

  - Automated the process via GitLab CI 
  - Published sample `urlwatch` configs for monitoring changes

## Who cares?

Consider Kushagra Pathak's work on: [How I used a simple Google query to mine passwords from dozens of public Trello boards](https://medium.com/free-code-camp/discovering-the-hidden-mine-of-credentials-and-sensitive-information-8e5ccfef2724). It ultimately led to this write-up in **The Intercept**: [United Nations accidentally exposed passwords and sensitive information to the whole Internet](https://theintercept.com/2018/09/24/united-nations-trello-jira-google-docs-passwords/). 


  - It's trivially easy to find public Trello boards. It's easy for your team and it's easy for the adversary. 
  - Trello has increasingly turned toward addressing the enterprise use case. 

Be aware of your Trello board footprint before it becomes a problem. 

If you are using Trello in your organization, you may wish to have some process that regularly checks to see what public boards are present in your environment and lets you make sure this list only contains the boards that you expect. This post describes an approach to solving this problem and some code you can borrow, modify and implement. 

## Preparation

Before you run `trello-public-board-lister`, you need to have a list of Trello usernames and a Trello API key and token. 

### Provide a list of usernames

Put the list of usernames you want to monitor in a file, one-per-line. 

### Get Trello API key and token

Log in to Trello and then browse to [https://trello.com/app-key](https://trello.com/app-key).

  - Record the **Key** that is shown securely
  - Click **Token**, follow the dialogs and record the **Token** that is shown securely


## Run trello-public-board-lister

Run `trello-public-board-lister` manually to make sure everything works as you expect and then automate the process with a CI pipeline. You'll want to run all of these commands in the same terminal session. 


```markdown
git clone git@gitlab.com:brie/trello-public-board-lister.git
```

### Run manually

I recommend using a Python virtual environment. Instructions on setting up a virtual environment don't belong in this blog post but Corey Schafer has [fantastic Python videos](https://www.youtube.com/watch?v=Kg1Yvry_Ydk) on Python. 

Change into the cloned directory, create a new virtual environment, activate it and install the required Python packages for `trello-public-board-lister`:


```markdown
cd trello-public-board-lister
python3 -m venv v
source v/bin/activate
pip install -r requirements.txt
```

#### Make the Trello API key and token available

You have two options:

  - environment variables 
  - command line arguments

I recommend using [environment variables](https://wizardzines.com/comics/environment-variables/). 

```markdown 
export TRELLO_API_KEY=yourtrelloapikey
export TRELLO_API_TOKEN=yourtrelloapitoken
```

### OK, go! 

Everything should be all ready. Copy your list of Trello usernames into this directory. Run:

```markdown
python3 trello-public-board-lister.py --usernames trello-usernames.txt
```

You should see output like this:


![output](/posts/run-trello-public-board-lister00.png)

For each username provided, you will have a `.boards` file containing a JSON object that includes the URL for each public Trello board for that user. You can use `jq` to count the URLs in each file:

```markdown
$ jq '. | length' taco.boards
8
```

If everything is looking OK, you can move on to automating and scheduling. Feel free to fork [trello-public-board-lister]() and patch it to work to better meet your needs. 


### Automate the process with a CI pipeline

Let's set up GitLab to run this whole process as a [CI pipeline](https://docs.gitlab.com/ee/ci/pipelines/) on a [schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html). 

Create a project in your GitLab instance for this purpose. 

##### Add Trello API key and token as masked variables in GitLab

We are going to continue using environment variables to tell `trello-public-board-lister.py` about our Trello API key and token. When running this program in a GitLab pipeline, we do this by defining masked custom environment variables. For both `TRELLO_API_KEY` and `TRELLO_API_TOKEN`, do:

  1. Hover over **Settings** and then click **CI / CD**
  1. Expand **Variables**
  1. Click **Add Variable**

The **Key** should be: `TRELLO_API_KEY`
For the **Value**, copy in your Trello API key
Uncheck **Protect variable**
Make super sure that you click **Mask variable**

When that's all done: click **Add variable**

It should look something like this:

![masked custom environment variables](/posts/masked-custom-environment-variables.png)

The environment variables are all set! 

### Build a pipeline

I have a sample `.gitlab-ci.yml` file that you can modify and use to run this pipeline via GitLab:

  - [.gitlab-ci.yml](https://gitlab.com/brie/check-for-public-trello-boards)  from `gitlab.com/brie/check-for-public-trello-boards`

### Schedule the pipeline

You can [configure a pipeline schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#configuring-pipeline-schedules) so that everything runs and checks for public Trello boards at the frequency that is right for you. 

### Check for changes

Once you have an initial list of boards that are public, you may wish to check for changes in that list. I use `urlwatch`, a tool that [monitors webpages for you](https://github.com/thp/urlwatch) for this purpose. 

  - add a [job](https://urlwatch.readthedocs.io/en/latest/jobs.html) to check for changes in list of boards
  - Send changes by email via [built-in Mailgun reporter](https://urlwatch.readthedocs.io/en/latest/reporters.html#built-in-reporters)


## Recap

If you played along, you now have a process that checks for public Trello boards from a list of users that you care about. This process runs periodically and alerts you to changes in the list of public Trello boards. 