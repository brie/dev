---
title: "February 2023 - Good Reads + GraphQL, k8s for Cats and ChatGPT in Academia"
description: "A round-up of interesting things that I read, shipped and thought (a lot) about in February 2023."
excerpt: "A round-up of interesting things that I read, shipped and thought (a lot) about in February 2023."
slug: 2023-february-chatgpt-k8s-graphql
publishDate: 2023-02-28 23:59:21
tags: ['graphql','k8s','kubernetes','chatgpt','rce','security','cats','http']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/festive-february.jpg
fullscreen: true
created: 2023-02-04T21:22:34-05:00
modified: 2023-02-28T23:59:21-05:00
---

> Photographed relaxing above on his couch is my [cat, Plop](https://www.instagram.com/plop_is_a_cat/).

Hello, world! 

This is the second of twelve planned posts. (You can [read the first](https://brie.dev/2023-january-articles-notes-commands/).) This is an experiment in public goal-setting and I am pleased to be on-track -- **so far**.  I'll tell you briefly about what's been on my mind this month to help you determine whether this post and the rest of the series will be of interest to you:

  - GraphQL
  - Kubernetes
  - technical leadership
    - change
    - uncertainty

While I am very much a "hands on the keyboard" kind of person, we are human beings. Things in the "technical leadership" category like how to embrace change and deal with uncertainty are at least _as_ important as remote code execution vulnerabilities. Keep reading if you are interested in GraphQL, Kubernetes, infosec, cool tools and all of the human stuff that goes along with working in tech. I tend to write about what's on my mind during the course of a given month: it won't all be hyper-recent current events stuff. Discussion of things like ChatGPT will very likely soon show their age but there are many less time-sensitive topics here, too. 

--
Brie 

P.S. I am inclined to publish these posts via email. Buttondown seems a reasonable choice. Subscribe at [buttondown.email/brie](https://buttondown.email/brie) if you want to see these in your inbox once per month in 2023. 

# 🔖 Good Reads

>> Take a few moments, grab a notebook and read through these articles!

  - [LastPass says employee’s home computer was hacked and corporate vault taken](https://arstechnica.com/information-technology/2023/02/lastpass-hackers-infected-employees-home-computer-and-stole-corporate-vault/)
	  - Well, ars filed this under **The Hits Keep on Coming**.
	  - It [seems likely](https://www.vice.com/en/article/xgye3k/lastpass-shouldnt-be-trusted-with-your-passwords) that an unpatched Plex instance is the third-party media software package mentioned.
	  - The [linked toot thread](https://infosec.exchange/@epixoip/109585049354200263) on "why _not_ to use LastPass" predates the latest revelation.
	  - I'm following [this Twitter thread](https://twitter.com/_MG_/status/1630296865131630592) with interest.
  -  [I want to lose every debate.](https://sive.rs/led)
- Aaron Parecki's [life stack](https://aaronparecki.com/life-stack/)
- [Email: explained from first principles](https://explained-from-first-principles.com/email/)
- [The curious case of the disappearing Polish S](https://medium.engineering/the-curious-case-of-disappearing-polish-s-fa398313d4df): One keyboard bug three decades in the making!
	- I would both love and hate to track down something like this!
- [How to build an in-house on-call training program](https://fiberplane.com/blog/how-to-build-an-in-house-on-call-training-program)
- [Exploring Prompt Injection Attacks](https://research.nccgroup.com/2022/12/05/exploring-prompt-injection-attacks/)
	- This is more useful for fun than profit at present. 
	- [You can’t solve AI security problems with more AI](https://simonwillison.net/2022/Sep/17/prompt-injection-more-ai/)

# 🔒 RCE of RCE

>> Really Cool Example(s) of  Remote Code Execution 

I **love** a good write-up especially when remote code execution is involved! (Who doesn't?) My favorite read this month:

  - [Cacti: Unauthenticated Remote Code Execution](https://www.sonarsource.com/blog/cacti-unauthenticated-remote-code-execution/)

Even if this kind of thing is not your typical jam, I would recommend reading this one. It's pretty easy to follow along even if you are not already familiar with PHP (!) or Cacti.

This was a real blast from the past for me: Cacti is one of the very first monitoring tools I ever learned about. (It was an obvious choice in an [RRDtool](https://oss.oetiker.ch/rrdtool/) shop.)  

_N.B. It might not be THAT cool but I could not miss the opportunity for wordplay._

# 🌐 Mindmapping in Markdown

>> Markmap = Markdown + Mindmap

I am rather fond of `markmap` for turning Markdown into pretty mindmaps in the form of interactive HTML! You can see an example at [markmap.brie.dev](https://markmap.brie.dev/) or fork [my project](https://gitlab.com/brie/markmap-gitlab-pages) to get started with your own mindmap. While you _can_ bring existing Markdown to `markmap`, you might need to tweak it a bit based on how `markmap` decides what to render. Take a look at [the frontmatter](https://gitlab.com/brie/markmap-gitlab-pages/-/blob/main/README.md?plain=1) of the example for a few ways to modify the behavior of `markmap` instead of the content of your Markdown. 

I [tooted](https://infosec.exchange/@brie/109911582961289357) about this. 

## 💨 On tooting

Before writing this, I would roll my eyes in bemusement at the usage of `toot` on Mastodon. I wondered:

...but why?

The answer is pretty interesting, as it turns out:

It's no longer _officially_ called tooting. In the **merged** [Change the nouns "toot" and "status" to "post"](https://github.com/mastodon/mastodon/pull/16080) PR, the [lead developer](https://mastodon.social/@Gargron) of Mastodon [points](https://github.com/mastodon/mastodon/pull/16080#issuecomment-823413923) to [the very thread](https://mastodon.social/@Hbomberguy/146524) from 2016 where the whole tooting thing started: a dare from YouTuber and Twitch streamer [Hbomberguy](https://en.wikipedia.org/wiki/Hbomberguy). The [referenced Patreon](https://www.patreon.com/mastodon) is still active.

After reading the comments in [the PR](https://github.com/mastodon/mastodon/pull/16080) , I get it and don't mind "toot" so much but it still takes a little getting used to. It does make me smile so I'll take it. 

# 🎒 Noteworthy Uses of ChatGPT: Academia

>> In this moment, a semi-regular section on this topic seems worthwhile. If nothing else, it presents the opportunity for an interesting time capsule. 

According to the friend of someone on Reddit (😂), a [history professor **purportedly**](https://www.reddit.com/r/ChatGPT/comments/117gtom/my_friend_is_in_university_and_taking_a_history/) has students correct essays written by ChatGPT:

> My friend is in university and taking a history class. The professor is using ChatGPT to write essays on the history topics and the students need to mark up its essays and point out where ChatGPT is wrong and correct it.

What a cool idea! 

In [My class required AI. Here's what I've learned so far.](https://oneusefulthing.substack.com/p/my-class-required-ai-heres-what-ive) and the associated paper [New Modes of Learning Enabled by AI Chatbots: Three Methods and Assignments](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4300783) we find a more credible account of ChatGPT being embraced in the classroom. 

We also have this story:

  - [Vanderbilt University Staff Apologized For Using AI To Write An Email To Students About The Shooting At Michigan State](https://www.buzzfeednews.com/article/pocharaponneammanee/vanderbilt-email-chatgpt-ai-msu-shooting)

A [follow-up statement](https://peabody.vanderbilt.edu/statement.php) is available but given the `https://peabody.vanderbilt.edu/statement.php` URL and the link rot that seems inevitable, it's  [archived](https://archive.is/8w1fL) for future reference. (The [original email](https://t.e2ma.net/message/ul182h/m74zooz) is also [available in archive form](https://archive.ph/9DQk8).)

I will turn to an interesting side question that this raised for me. At the end of the letter, we see:

> (Paraphrase from OpenAI's ChatGPT AI language model, personal communication, February 15, 2023).

This was published by a university, an institution unlikely to take the matter of a citation unseriously.

  - **Q**: How does one cite ChatGPT (and friends)?

Unsurprisingly, a lot has been written on this topic! 

Bond University [notes](https://bond.libanswers.com/faq/273075) that responses from ChatGPT are considered **non-recoverable sources**. Other examples of non-recoverable sources include personal correspondence. Writers are encouraged to treat responses from ChatGPT and the like as personal communication or correspondence. It seems very reasonable to find and use something that already exists for this purpose. This will be an [interesting space](https://www.scribbr.com/ai-tools/chatgpt-citations/) to observe over time. 

Beyond the details of how to cite ChatGPT, the  [Implications in academia](https://en.wikipedia.org/wiki/ChatGPT#In_academia) section of the ChatGPT article on Wikipedia raises a few other interesting questions. 

# 🔍 GraphQL Security - in brief

If we have worked together or you know me IRL, we have _probably_ talked about GraphQL!

Compared to REST, GraphQL is [the grassy road wanting for wear](https://www.poetryfoundation.org/poems/44272/the-road-not-taken). A good ELI5 on the rough edges in GraphQL for pentesters, defenders and the curious can be found in the [GraphQL - Security Overview and Testing Tips](https://blog.doyensec.com/2018/05/17/graphql-security-overview.html) post. Observing the 2018 timestamp, I think that the age of the post is an advantage: a simpler description of GraphQL is provided and we can test whether the advice stood the test of time. Beyond that:

  - OWASP's [Web Security Testing Guide](https://owasp.org/www-project-web-security-testing-guide/v42/4-Web_Application_Security_Testing/12-API_Testing/01-Testing_GraphQL) has a section on testing GraphQL that may be of interest
  - the HackTricks book has a [section on GraphQL](https://book.hacktricks.xyz/network-services-pentesting/pentesting-web/graphql) that's great if you have a basic overview of what GraphQL is and want to get started **now**. 
  - If you are lucky, you will find that [GraphQL introspection is enabled](https://www.apollographql.com/blog/graphql/security/why-you-should-disable-graphql-introspection-in-production/) but [clairvoyance](https://github.com/nikitastupin/clairvoyance) offers a way to obtain a GraphQL API schema even if introspection is not enabled. (Introspection is a way of asking the GraphQL API to give you the schema. If the schema is public: this _may_ not be super helpful but it could be worthwhile to compare the published schema with what introspection tells you. You just might find something interesting.) 
   
However you get the schema: the `graphql-path-enum` tool described in [GraphQL path enumeration for better permission testing](https://blog.deesee.xyz/graphql/security/2020/04/13/graphql-permission-testing.html) looks promising!

# 😼 Deploying the HTTP Cats to Kubernetes, Part 1

Last year, I launched the [HTTP Status Cats](https://httpcat.us). There's [more about it here](https://brie.dev/httpcats/) but in short: it's a Flask app that serves cute cat pictures to remind you what different HTTP status codes stand for. (Check out [httpcat.us/404](https://httpcat.us/404) or [httpcat.us/502](https://httpcat.us/502) to see what I mean.) I am extending it for use in a Kubernetes cluster simply for the exercise. (There's no real need for these cats to be on a cluster except to make it cuter.) 

I am not a cloud-native software engineer and this is just for fun and experimentation so I'm taking the "can I make this thing work?" approach.

First things first: I need to write some YAML that I can pass to `kubectl apply` to have it tell the Kubernetes cluster to spin up the cats. 

  - **Input**: a Flask app | [source](https://gitlab.com/brie/httpmeow)
  - **Output**: Kubernetes manifest

After some research, trial, error, trial and success, I decided on [kompose](https://kompose.io) to solve this problem. I wrote a bit more about [using `kompose` to generate a k8s manifest for a Flask app](https://til.brie.dev/k8s/kompose-generate-kubernetes-manifest-for-flask-app) on my TIL (today I learned) site. Since `kompose` works best with Docker Compose and I was just using `Dockerfile`, I assembled this `docker-compose.yml`:

```yaml
# docker-compose.yml
version: '3'

services:
  httpmeow:
    build: '.'
    image: registry.gitlab.com/brie/httpmeow:latest
    ports:
      - '5000:8008'
```


The output of `kompose` included a [deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) as well as a [network policy](https://kubernetes.io/docs/concepts/services-networking/network-policies/) and a [service](https://kubernetes.io/docs/concepts/services-networking/service/). The deployment is the most important thing for now. See [the YAML](https://gitlab.com/brie/httpmeow/-/blob/dev/k8s-part1/k8s/httpmeow-deployment.yaml) or generate it yourself with:

```shell
git clone https://github.com/kubernetes/kompose.git
cd kompose && docker build -t kompose:v0 .
cd ..
git clone https://gitlab.com/brie/httpmeow.git
cd httpmeow
docker run --rm -it -v $PWD:/opt kompose:v0  sh -c "cd /opt && kompose convert"
```

In part 2, we'll look at different options for deployment and use that to guide what we do with the network policy and the service. (If we use something like Okteto, we'll have to work within what's available to us.)

# ❤️ From the Heart

In January, I [wrote](https://brie.dev/2023-january-articles-notes-commands/) that moments of happiness should be savored when they are encountered. It is possible to create and foster conditions that are likely lead to moments of happiness. Other times, the moments of happiness appear by surprise and these unexpected moments of joy on tough days are a [Giladean balm](https://en.wikipedia.org/wiki/Balm_of_Gilead_(disambiguation)). (Is that the right demonym?) 

On a very bad day, I found comfort, wonder and joy in this flower-filled lovely gentle introduction to Minecraft: 

  - [HeimatKraft #1 | Starting at the Big Hole](https://youtu.be/-IOJL6iGRrM)

I was rather ignorant of Minecraft before this: I knew there were blocks, you could build worlds and different people ran Minecraft servers. That's about it. 

While the creator is a good friend, I happened upon the video organically and was really moved by the care and passion.  Enjoy!
 
Be well!

--
Brie 🦄🌈
