---
title: "November 2023 - 🔐 OCSP Response 🧪 Testing with k6"
description: "Using k6 to check if http.OCSP_STATUS_GOOD."
excerpt: "ICANHAZOCSP?"
highlight: "is OCSP response good?"
slug: 2023-ocsp-k6
publishDate: 2023-11-15 01:31:21
tags: ['security','ocsp','cloud','testing','http','https','tls','ssl','pki','x.509','x509']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/imuststaple.jpg
fullscreen: true
created: 2023-11-15T01:22:34+06:00
modified: 2023-11-258T01:49:21+06:00
---

# 🔐 OCSP Response Testing with k6

In `thisUpdate`, we'll learn about Online Certificate Status Protocol (**OCSP**) and how to validate OCSP responses with `k6`.

Grafana [k6](https://k6.io) is a load testing tool for engineers that can be used on `localhost`, in [💚 CI pipelines](https://docs.gitlab.com/ee/ci/testing/load_performance_testing.html) or via Grafana ☁️  Cloud. 

[k6 supports OCSP stapling.](https://k6.io/docs/using-k6/protocols/ssl-tls/online-certificate-status-protocol-ocsp/) The [SSL Pulse](https://www.ssllabs.com/ssl-pulse/) dashboard from Qualys SSL Labs reports that 49.2% of sites surveyed support OCSP stapling.

**OCSP stapling** is a privacy-preserving approach to checking whether a TLS certificate has been revoked. Timestamped OCSP responses are presented by the server during the TLS 🤝 handshake.


## 🚀 Let's start testing!

With [the `k6` executable installed](https://k6.io/docs/get-started/installation/), a script like the following can be used to check OCSP responses:

```
import http from 'k6/http';
import { check } from 'k6';

export default function () {
  const res = http.get('https://brie.dev');
  check(res, {
    'is OCSP response good': (r) => r.ocsp.status === http.OCSP_STATUS_GOOD,
  });
}
```

By convention, `k6` load testing files have a `.js` extension. 🚀 Run `k6 run --quiet whatever.js`.

```
 k6 run --quiet   whatever.js

     ✓ is OCSP response good

     checks.........................: 100.00% ✓ 1         ✗ 0
     data_received..................: 30 kB   477 kB/s
     data_sent......................: 715 B   12 kB/s
...
     iterations.....................: 1       16.033349/s
```

I ♥️ love [the various subdomains](https://badssl.com) over at `badssl.com` for testing bad SSL configurations. Modifying the `k6` script from earlier to point at `revoked.badssl.com` shows how test failures are reported:

```
# k6 --quiet run revoked.js
WARN[0000] Request Failed                                error="Get \"https://revoked.badssl.com/\": tls: failed to verify certificate: x509: certificate has expired or is not yet valid: “revoked.badssl.com” certificate is expired"

     ✗ is OCSP OK
      ↳  0% — ✓ 0 / ✗ 1
...
```

### 📖 OCSP Terminology

A basic grasp of these concepts is recommended. These links are selected to help teach or remind the reader:

- [Certificate Revocation](https://www.ncsc.gov.uk/collection/in-house-public-key-infrastructure/pki-principles/use-certificate-revocation)
- [OCSP Must-Staple](https://scotthelme.co.uk/ocsp-must-staple/) | A certificate extension that tells the browser to expect a valid OCSP response "stapled" (included) in the handshake.
- OCSP Request | The request to determine the revocation status of an X.509 certificate. With OCSP, the request is made by the client. With OCSP stapling, the request is made by the Web server and the response is included in the handshake.
- [OCSP Responder/OCSP Server](https://techcommunity.microsoft.com/t5/ask-the-directory-services-team/implementing-an-ocsp-responder-part-i-introducing-ocsp/ba-p/396493) | The party responsible for checking the status of the X.509 certificate with a certificate authority _and_ sending the OCSP Response to the requester. 
- OCSP Response | The response that tells the client the OCSP status and whether or not the certificate is valid.
- [OCSP Stapling](https://www.rapidsslonline.com/ssl/what-is-ocsp-ssl-stapling/) | Stapling improves OCSP by putting the burden of retrieving the OCSP Response on the Web server (rather than the client).

This is just an introduction: you can further improve performance by implementing a [caching OCSP proxy](https://github.com/philfry/ocsp_proxy).

### 📚 READmore

A few excellent articles and resources I read while writing this blog post, organized with my favorites towards the top: 

- [OCSP Validation with OpenSSL](https://akshayranganath.github.io/OCSP-Validation-With-Openssl/) - a very nice article by [Akshay Ranganath](https://www.linkedin.com/in/akshayranganath/) 
- 🐞 [Fetch OCSP responses on startup, and store across restarts](https://trac.nginx.org/nginx/ticket/812)
- ⭐️ [ocsp-stapling.md](https://gist.github.com/sleevi/5efe9ef98961ecfb4da8) - Ryan Sleevi's notes on requirements for OCSP stapling support. ⭐️
- 😍 APNIC has a series of blog posts on OCSP stapling starting with [Overcoming the limitations of OCSP](https://blog.apnic.net/2019/01/11/overcoming-the-limitations-of-ocsp/).
- [High-reliability OCSP stapling and why it matters](https://blog.cloudflare.com/high-reliability-ocsp-stapling/)
- [OCSP Server for Google Cloud Certificate Service](https://github.com/GoogleCloudPlatform/gcp-ca-service-ocsp/blob/master/README.md)
- [OCSP_resp_find_status](https://www.openssl.org/docs/man1.1.1/man3/OCSP_resp_get1_id.html)
- [RFC 2560](https://www.ietf.org/rfc/rfc2560.txt) | X.509 Internet Public Key Infrastructure Online Certificate Status Protocol - OCSP
- [OpenSSL: Manually verify a certificate against an OCSP](https://raymii.org/s/articles/OpenSSL_Manually_Verify_a_certificate_against_an_OCSP.html)
- [OCSP Configuration](https://docs.snowflake.com/en/user-guide/ocsp) - Snowflake
- [Implementing an OCSP responder: Part I - Introducing OCSP](https://techcommunity.microsoft.com/t5/ask-the-directory-services-team/implementing-an-ocsp-responder-part-i-introducing-ocsp/ba-p/396493)

💖 Be well, you.
