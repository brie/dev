---
title: "Using Markdown for Mindmaps with Markmap"
highlight: "Markmap Makes Markdown Mindmaps"
description: "Generating mindmaps with Markdown: HTML export included!"
excerpt: "Generating mindmaps with Markdown: HTML export included! Say 'markmap makes markdown mindmaps' three times fast."
slug: 2023-markdown-mindmap
publishDate: 03/23/2023
tags: ['markdown','mindmap','brainstorm','tools','ci','html','web','utilities','notes','notetaking']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/like-like.jpg
fullscreen: true
---

> Above is a photograph I took in January 2023 of the the facade of [LIKELIKE](https://likelike.org/) in Pittsburgh, Pennsylvania.

Hi, there. Do you like mindmaps, Markdown, open source software _and_ having an excuse to make another website? If **yes**: right this way, please! I recently published [a project](https://gitlab.com/brie/markmap-gitlab-pages) that [demonstrates](https://markmap.brie.dev) how to use Markdown to generate delightful interactive mindmaps that can be exported to HTML or SVG[0]. The tool that does the heavy lifting is [markmap](https://markmap.js.org/). Here's an example of the output it generates:

![](/posts/markmap-example.png)

See it live at [markmap.brie.dev](https://markmap.brie.dev) or take a peek at the [source](https://gitlab.com/brie/markmap-gitlab-pages/-/raw/main/README.md). There is a [Markmap REPL in the Web playground](https://markmap.js.org/repl) if you want to get hands-on ⚡️ now ⚡️. Alternately, keep reading for a few quick steps to get a website like the one at [markmap.brie.dev](https://markmap.brie.dev) made **for you** via GitLab CI. 

## Make your own mindmap! 

You'll need an account on `gitlab.com` or a self-managed GitLab instance in order to follow along. Some experience with GitLab CI or enough curiosity to get through are recommended.

  1. 🍴  Fork (or [import](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html)) the `brie/markmap-gitlab-pages` [project](https://gitlab.com/brie/markmap-gitlab-pages).
  1. ✍️  Update `README.md` with whatever content you'd like.
  1. 💚  Let the pipeline finish.
  1. 🖱  Browse to **Deployment** > **Pages** to retrieve the URL for your new site.  
  1. 🎩  (Optional) Add a [custom domain](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/) while you're at **Deployment** > **Pages**.
  1. 🍵  Enjoy! 

## How I use `markmap` + using `markmap` with existing content

I have found `markmap` a great tool for traditional mindmapping activities like brainstorming and notetaking. There are a [number of options](https://markmap.js.org/docs/json-options#option-list) available for customizing `markmap` in the [frontmatter](https://daily-dev-tips.com/posts/what-exactly-is-frontmatter/) of the Markdown file. I used most of the options in the example `README.md`. 

Since `markmap` accepts Markdown, it is possible to easily make a mindmap from existing content. The [Web playground](https://markmap.js.org/repl) means this can be as easy as pasting the Markdown into a browser tab. A word of caution: `markmap` has its own way of deciding what gets a bubble and what gets hidden. When you import existing content, you might find that some things don't quite appear as you'd like. You'll have to decide whether it's worth changing the structure of the document to get `markmap` to play along.

📯 Feel free to [toot me your thoughts](https://infosec.exchange/@brie).

--
Brie 🦄🌈

[0] - SVG will always be interesting and exciting to me. I attribute this to the delight that was spending a few years learning from the inimitable [Dr. David Dailey](http://srufaculty.sru.edu/david.dailey/indexO.htm) during my undergraduate studies. 

> ```
> FIN
> ```
