---
title: 'April 2023 - A Staff (Support) Engineer''s Path'
highlight: A Staff (Support) Engineer's Path
description: A collection of notes and reflections after reading and discussing The Staff Engineer's Path.
excerpt: A collection of notes and reflections after reading and discussing The Staff Engineer's Path.
slug: staff-engineer-path
publishDate: 2023-04-30 20:53:31
tags: [support, leadership, book, club, work, reading, reflections]
author: brie-carranza
authors: Brie Carranza
image: ~/assets/images/posts/plumeria.jpg
fullscreen: true
---

> Above is a photograph of me holding a fallen plumeria during my September 2022 visit to Kualoa Ranch on the windward coast of O'ahu.

Hello, world!

In spring 2023, I was **delighted** to participate in a [book club](https://about.gitlab.com/handbook/leadership/book-clubs/) at [work](https://gitlab.com) where we read Tanya Reilly's [The Staff Engineer's Path](https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/). The book, subtitled **A Guide for Individual Contributors Navigating Growth and Change**, was ✨ excellent ✨. In time, I am sure I will look back at the experience as foundational. I had been looking for this kind of material and I found it at a really exciting time in my professional career. (I'll be reflecting on 3️⃣ three years at GitLab next month!)

 I've reflected quite a bit on the book in private writing, in [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with new friends, on [this site](https://staffeng.carranza.engineer/), on [YouTube](https://www.youtube.com/watch?v=ygYyvPVwsRk) and in asynchronous discussions with [a dear friend](https://www.linkedin.com/posts/manuel-grabowski_supportengineer-experience-quality-activity-7052677414413307905-v4UI). In this post, I wanted to share some of the 🔑 key lessons from the experience that really stuck with me. 
 
 A quick list to help you decide if this blog post is of interest to you at this time:

  - Someone should do something
  - Finite time
  - Embrace uncertainty
  - Level up the team
  - Use your influence **at scale**
  - A few points from the book that I keep thinking about

Let's go!

## 🎬 Someone should do something

If you look at a situation and think "someone should do something", that someone is probably you. (Sorry...)

Here's where it gets tricky:

  - You don't have to do every thing
  - You should not do _any_ thing
  - You should do **some** thing

Sometimes, the "something" is to _knowingly_ ignore the problem (for now and possibly forever). Keep reading for more about the decisions we must make given the finite nature of the time before us. 

## 🕰 Finite time

The book's discussion of finite time brings a sense of urgency to the decisions made every single day. The importance of choosing how to spend one's time with care is made clear. A friend put it succinctly:

> You can not solve every problem

You know that already. Intellectually, this is easy and obvious. However, this is very difficult to keep in mind when the problem you can't spend the time to solve is **right there** and **super painful** and you already have a few ideas. What if we just... 🛑 Stop.

In a way, the discussion on **finite time** that runs through the book is  a way of reckoning with mortality in an even more pressing way. The finite nature of my time on Earth has been clear to me for some time but the discussion in the book provided a new framing that has been very impactful for me professionally and personally. 

🎗 Remember: the finite time available to you elapses regardless of how intentionally you spend it.  

## 🤗 Embrace uncertainty

Things are messy. There is no clear path forward. There is no process for how to do _that_. 

The absence of a clear path forward doesn't mean you leave that trail unexplored! It means you **accept** and even **embrace** that there is a lot of uncertainty ahead. It means proceed -- but with ⚠️ (an appropriate amount of) caution. Uncertainty means there is an opportunity for creative problem-solving (exciting!). It can also be indicative of fewer restraints to work within. In other words: uncertainty is not all downside.  

  - Come to terms with the fact that you'll have to make _a_ decision
    - You might get it wrong
    - Learn what you can from the experience and move on


#### ⚜️ Gold Rush

This reminds me of something I saw on a trip to Alaska in 2019 that has stuck with me. I share this story frequently and am excited to have it written here for future reference:

During the Klondike Gold Rush, prospectors had to choose between one of two trails. The shorter trail meant prospectors would get there more quickly and spend less money on rations for the trip but the path was treacherous and perilous. The longer trail was safer (by comparison) but would lengthen the journey, allowing more time for others to get to the gold first and increasing the expense.

In The Yukon, there is a quote from a prospector who assessed the situation like this: 

> Whichever way you go, you will wish you had gone the other.

In a way: that's normal and fine. Don't dwell there, though. 

![](/posts/yukon-suspension-passport.jpg)

> Foreground: the **Yukon Suspension Bridge** stamp I got in my passport. So exciting! 🎉 Background: the **Yukon Suspension  Bridge** itself. September 2019.

#### 🥪 Suboptimal Options

There will be times when there is no clear, correct and excellent answer. Perhaps every available option has some (major!) drawback. The best you can do in these situations is work to identify the best **available** solution.

## 🎚 Level up the team

Self-improvement is a continuous journey but the focus should not be entirely on the self. Help **forge** other senior engineers! The adage about teaching someone to fish rather than catching the fish for them comes in here. Here's how I think about it: 

When someone asks for help, consider how to do **both** of the following:

  - 🎫 Helpm them solve the immediate problem that they requested help with
  - 🎒 Ensure they have the resources, knowledge,  and insight to help solve problems  **like** that one in the future

It might lengthen the amount of time required to solve the immediate problem but it's absolutely worth the time investment to ensure they understand a bit more about how the _thing_ works. When you understand how someone arrived at the answer, you are better equipped to handle follow-up questions on your own and you have helpful information for the next ticket that looks like this one. "Oh, yeah, I remember when Soandso helped me with `x` a few months ago." I know I benefited from the times when people did the same for me. (Thank you. 🙏 )

## ⚖️ Use your influence **at scale**

At times, I find it helpful to think about **quiet** leadership and **loud** leadership. 

  - Quiet leadership is a 1:1 sync where you let someone vent and then help them assemble a path forward.
  - Loud leadership includes what the author describes as using one's influence at scale: blog posts and talks are great examples.

Both are very important! (Not _every_ thing fits neatly into these categories.) Find the right balance for you (your skill set and the needs of the team around you). 

It can feel so _awkward_ to promote oneself. 🦁 A bit of bravery and inward reflection 🪞 may be required. "Who cares what I think?" Some people will care and others will not. Oh well! You reached some people; **may that be enough**. 

## 📝 A few notes I keep thinking about

  - **Establish** and **maintain** relationships outside your team and your organization.
    - Call it [networking](https://xkcd.com/1032/) if you like or don't but...it's important.  
  - You have **influence**. Use it for good: the good of the team...and beyond! 🚀  
  - Be **approachable**. Folks will come to you for guidance and assistance: ensure it is worth their while. 
    - Be helpful! 
    - Answer the direct question. 
    - Address any implicit asks, too.  
  - Level up the team. 
    - Keep your skills sharp but consider how you can bring the team up with you. 

# 🐾 Next Steps

✅ Check out [staffeng.carranza.engineer](https://staffeng.carranza.engineer/) where I'm keeping track of my notes on the book in particular and on staff (support) engineering in general. Last month, I [wrote](https://brie.dev/2023-markdown-mindmap/) about using `markmap` to generate mindmaps. I made mindmaps for a few chapters in the book. I might recommend the [mindmap for the intro and the first chapter](https://brie.gitlab.io/staffeng/maps/chapter1.html) to start. 

For me: I intend to continue letting the lessons from the book guide my actions. I keep the book handy for reference and I intend to read it again in a few months.

For you: I _highly_ recommend reading this book if you work **with** a staff engineer, whether they are a leader on your team, your direct report or simply a fellow team member. Understanding what staff+ engineers are trying to do will help everyone.

🎗 A note and a reminder: I titled this post intentionally. Just as there are many different _kinds_ of staff engineer, there are many pathways toward becoming a staff engineer. I _highly_ recommend reading what the inimitable and amazing [Cynthia "Arty" Ng](https://about.me/cynthiang) has [written on this topic](https://cynthiang.ca/2022/11/04/reflection-first-third-of-my-fifth-year-at-gitlab-and-defining-staff-support-engineer/). These are notes on what makes sense to me as I find my way along an ongoing journey, in the hopes you find some value as you build your own path.

> Today is your day!
>
> Your mountain is waiting.
>
> So...get on your way!

...from [Oh, the Places You'll Go!](https://www.goodreads.com/quotes/495178-and-will-you-succeed-yes-you-will-indeed-98-and)

# ❤️  From the Heart

You need not be perfect to be a leader.

> And the moon said to me - my darling daughter, you do not have to be whole in order to shine.
> 
  - Nichole McElhaney

💖

--
Brie 🌈 🦄
