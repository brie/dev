---
title: "Python Made Better with uv"
description: "Thoughts and notes after a month using uv."
excerpt: "Thoughts and notes after a month using uv."
highlight: "Thoughts and notes after a month using uv."
slug: python-better-with-uv
publishDate: 2025-01-31 23:32:31
tags: ['pip','pypi','python','uv']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/android-payphone.jpg
fullscreen: true
---

> I won the pictured Android figurine in a computer science trivia contest.


I have been happily using Python for a variety of purposes since version  `2.5`. As a result, my Python workflow is very important to me. When I first heard about `uv`, I was skeptical that it would actually be a drop-in replacement for `pip` (and friends) for me. I decided to spend January 2025 giving it a genuine try and I am **so** very glad I did. For me, `uv` totally lives up to the hype!

⭐️✨ I think `uv` makes Python easier -- and more fun! ✨⭐️

# 🌞 What is `uv`?

In short, [uv is an extremely fast Python package manager, written in Rust](https://astral.sh/blog/uv-unified-python-packaging).

# ❓ Why uv?

Do you use Python and virtual environments? Do you find them _kinda_ annoying and cumbersome? 

👉 [Try uv](https://github.com/astral-sh/uv/releases). 👈

There are other good reasons to use `uv` but that's my elevator pitch. In this post, I want to walk through a few examples of how I am using `uv` to help illustrate why I am so excited about it. [Grab your copy](https://github.com/astral-sh/uv/releases) and follow along!

# 🗺️ How I am using `uv`


To get a Python console:

```shell
uv run python3
```

Spin up a Web server serving your local directory:

```
uv run python -m http.server
uv run python -m http.server 8888
```

## 📝  `requirements.txt`

Do you find yourself doing something like this?

```shell=
python3 -m venv v
source v/bin/activate
pip install -r requirements.txt
```

There is support using `pip install` with `uv`. Those three lines above turn into one:

```
uv pip install -r requirements.txt
```

You don't have to create or activate the virtual environment first (but you [can if you would like](https://docs.astral.sh/uv/pip/environments/#creating-a-virtual-environment)).

Whether or not you will actually need to do this will depend a bit on what you are trying to achieve. There's more about how `uv` handles dependencies below.


## 🧰 Using tools

You can use `uvx` to run Python packages that can be used as tools. Let's say you wanted to use [Waybackpy](https://pypi.org/project/waybackpy/) to save a page in the Wayback Machine. When you run `uvx` (an alias for `uv tool run`), you **bypass** the `pip install` step. This command will take care of installing **and** running the tool all in one go. 

```shell
uv tool run waybackpy --url https://brie.dev --save
```

Let's say you wanted to use the `pastebin-bisque` tool I maintain to download some files from Pastebin:

```shell
uvx pastebin-bisque -u thecutestcat
```

For some tools, the package name and the tool name don't match. You'll need `--from` for those situations. 

```shell
uv tool run --from mitmproxy mitmweb
```

```shell
uvx   --from norwegianblue  eol  --md   gitlab   | head -n5
| cycle |  release   | latest   | latest release |  support   |    eol     |
| :-----| :--------: | :--------| :------------: | :--------: | :--------: |
| 17.8  | 2025-01-16 | 17.8.1 |   2025-01-22   | 2025-02-20 | 2025-04-17 |
| 17.7  | 2024-12-19 | 17.7.3 |   2025-01-22   | 2025-01-16 | 2025-03-20 |
| 17.6  | 2024-11-21 | 17.6.4 |   2025-01-22   | 2024-12-19 | 2025-02-20 |
```


## 📜 Running scripts

Running scripts with `uv` is as simple as:

```
uv run python whatever.py
```

If the `whatever.py` script has dependencies, you'd run it with something like this:

```
uv run --with datetime,requests python whatever.py
```

The list of dependencies can get a bit unwieldy. Instead of using `--with`, you can inline the dependencies!

## 🛼 Inlining dependencies

This is not a feature in `uv` but it makes using `uv` so much nicer if you're already taking advantage of this Python functionality. Without it, you would have to run something like this:

```
uv run --with argparse,datetime,passgen,faker,requests python main.py 4 
```

With it, you would just run:

```shell
uv run python main.py 4
```

Rob Allen has a [great blog post](https://akrabat.com/defining-python-dependencies-at-the-top-of-the-file/) on how to do this.

## 📍 Getting a specific version of Python

With `--python`, you can request a specific version of Python. 

```shell
# uv run --python 3.12 python -V
Python 3.12.8
```

That command tells `uv` to use Python `3.12` when running `python -V` to print the version of Python that is in use. Most `uv` commands support this flag.

## 📦 Publishing Python Packages with `uv`

In [fall 2023](/2023-python-packaging), I wrote about building my toolbox for publishing Python packages to PyPI and GitLab Package Registry. I have been able to replace the majority of it with `uv`. (I include a separate tool for version bumping and that's fine.) 

# 📚 Additional Reading

- ⭐️ The [uv docs](https://docs.astral.sh/uv/) are great.
- [uv under discussion on Mastodon](https://simonwillison.net/2024/Sep/8/uv-under-discussion-on-mastodon/) -- [Simon Willison](https://simonwillison.net/) has collected excerpts from `uv` chatter on Mastodon.
 - [UV — I am (somewhat) sold](https://andrich.me/2024/09/15/uv-i-am-somewhat-sold.html)
- [Defining Python dependencies at the top of the file](https://akrabat.com/defining-python-dependencies-at-the-top-of-the-file/)

# 💖 From the Heart

🐍 I haven't been this excited about something new in the Python space in a very long time. As I get a handle on how I intend to use `uv` moving forward, I'll work on building aliases and helper scripts to make my usage of `uv` even more efficient.

🐈‍⬛ Be well!
