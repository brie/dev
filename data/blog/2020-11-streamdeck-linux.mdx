---
title: "Set up and control elgato Stream Deck Mini on Linux"
description: "Here's how I did it."
slug: 2020-11-elgato-stream-deck-mini-linux
publishDate: 2020-11-28 16:44:21
tags: ['rust','cargo','elgato', 'stream deck','usb','openmoji']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/rocks-in-wainfleet-ontario.jpg
excerpt: "Set up and control elgato Stream Deck Mini on Linux with streamdeck-cli."
fullscreen: true
---

I bought the elgato Stream Deck Mini. I wanted to get it set up on my desktop running [Parrot OS](https://parrotlinux.org/). It was ridiculously easy to get it working with the `streamdeck-cli` provided by the `streamdeck` package on [crates.io](https://crates.io). With that installed, I was able to set custom colors and images on each of the keys. 


## Prerequisites
### Get the serial number of your Stream Deck Mini

After you plug it in, run `dmesg` to get the Serial Number. Set the environment variable `USB_SERIAL` equal to your Serial Number so that you can copy and paste commands below. 

### Install Rust

The [quickstart](https://www.rust-lang.org/tools/install) instructions for Rust are as follows:

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

(Exercise caution.)

## Install streamdeck-cli

With Rust installed and `cargo` in your `$PATH`, it's time to install [streamdeck](https://crates.io/crates/streamdeck), from crates.io. 

```markdown
# cargo install streamdeck
```

This will provide the `streamdeck-cli` executable used to update the Stream Deck Mini. 



## Set the brightness

To make sure everything is working, set the brightness to `0`:

```
streamdeck-cli set-brightness 0
```

Set the brightness to `100`:

```
streamdeck-cli  set-brightness 100
```

## Set button colors

In order to use the `set-colour` subcommand, you need to specify the key you want to modify. The keys are 1 through 6, like so:

```
| 1 | 2 | 3 | 
```
```
| 4 | 5 | 6 | 
```

You also need the RGB value for the color you want to use. You can easily convert [hex to RGB](https://hex2rgb.online/) online. 

### Set button 4 (lower left) to Twitter blue

```
streamdeck-cli set-colour 4 --r 29 --g 161 --b 242
```

### Set all six buttons to Overwatch orange in one go

With the magic of the `seq` command, you can set all six buttons to the same color like so:

```
for i in $(seq 1 6) ; do  streamdeck-cli set-colour  $i  --r 249 --g 158 --b 26  ; done
```


## Set images

In order to use the `set-image` subcommand, you need to specify the key you want to modify and the path to an image you want to use. 

I decided to create to use six different cat emoji from Openmoji:

  - [cats on openmoji.org](https://openmoji.org/library/#group=smileys-emotion%2Fcat-face)

Here's the idea I had in mind:

![cat key layout](/posts/cat-key-layout.png)


Once I had downloaded the files and renamed them, I ran these commands to set a custom cat face for each of the six buttons: 


```markdown
streamdeck-cli  set-image 1 1_laughing_cat.png
streamdeck-cli  set-image 2 2_crying_cat.png
streamdeck-cli  set-image 3 3_kissing_cat.png
streamdeck-cli  set-image 4 4_laugh_cry_cat.png
streamdeck-cli  set-image 5 5_pouting_cat.png
streamdeck-cli  set-image 6 6_smiing_cat_with_heart_eyes.png
```

Here's how the final product looks: 


![six cat faces](/posts/cat-faces.jpg)


A note on image selection:

The icons from OpenMoji will look good on the relatively small buttons on the deck. If you attempt to send an image that's too large, you'll see:

```markdown
[ERROR] Command error: InvalidImageSize
```

  * The largest image that I added was ~15K. 
  * The smallest image that has returned that error message is ~26K. 



## Limitations and Next Steps

The `streamdeck-cli` tool is really fun and easy but the only interesting subcommands are for setting brightness, colours and images. I found that I couldn't control both the color and the image. I'd like to go a little further and be able to receive keypresses. 

Here's what's next:

  - Check out [streamdeckd](https://github.com/unix-streamdeck/streamdeckd)
  - Try [streamdeck-ui](https://pypi.org/project/streamdeck-ui/)
  - USB captures via Wireshark and review [Streamdeck HID notes](https://gist.github.com/cliffrowley/d18a9c4569537b195f2b1eb6c68469e0) as necessary



## Appendix
### lsusb

Let's take a look at the `dmesg` output we see when connecting Stream Deck Mini:

```markdown
dmesg | grep Stream | grep devices
[1288627.009470] input: Elgato Systems Stream Deck Mini as /devices/pci0000:00/0000:00:14.0/usb1/1-5/1-5:1.0/0003:0FD9:0063.0006/input/input34
[1288761.853895] input: Elgato Systems Stream Deck Mini as /devices/pci0000:00/0000:00:14.0/usb1/1-5/1-5:1.0/0003:0FD9:0063.0007/input/input35
```


```markdown
# dmesg | grep Stream | grep devices   | cut -d"/" -f7
1-5:1.0
```

```markdown
# lsusb -s 1-5:1.0
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```
