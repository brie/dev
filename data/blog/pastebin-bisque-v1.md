---
title: "September  2023 - 🚀 Announcing pastebin-bisque v1.0"
description: "A quick update about the latest release of pastebin-bisque!"
excerpt: "My pastebin-bisque program is now available on PyPI!"
highlight: "🚀 pip install pastebin-bisque"
slug: 2023-september-pastebin-bisque-v1
publishDate: 2023-09-15 01:31:21
tags: ['python','development','software','packaging','utilities']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/pastebin-bisque.jpg
fullscreen: true
created: 2023-09-15T01:22:34+06:00
modified: 2023-06-308T01:49:21+06:00
---

# 🚀 Announcing pastebin-bisque v1.0

Earlier this week, I released `v1.0` of `pastebin-bisque`, the most popular piece of software I have written. You can get this version by running:

```shell
pip install pastebin-bisque
```

Once it's installed, run `pastebin-bisque --help` for more information. 

## ❓ What is `pastebin-bisque` and why would anyone use it?

✨ Great question! ✨ It's a program that downloads every public Pastebin post by a specific user. 

Let's say that there is a user on Pastebin who has posted a bunch of stuff that you are interested in. Instead of downloading it all manually, you can run `pastebin-bisque -u exampleusername`. That will download every public post that user has posted. 

### 👩‍🍳 Why was `pastebin-bisque` made?

I was doing some ransomware research and I was interested in the ransomware notes that [Michael Gillespie](https://www.propublica.org/article/the-ransomware-superhero-of-normal-illinois) had on [his Pastebin profile](https://pastebin.com/u/Demonslay335): it's an interesting, large public set of samples. I wanted an excuse to apply what I had been learning about [Beautiful Soup](https://pypi.org/project/beautifulsoup4/) and this Web scraping project presented the perfect opportunity.

## 🆕 What's new in `v1`?

For users: There are no changes in **functionality** in this version. The biggest difference for users is that you install the package from PyPI (`pip install pastebin-bisque`) and that you run the program with `pastebin-bisque`. All of the command line flags are the same. 

For contributors: there are a few significant changes and improvements. The program is now distributed as a Python package. The code that does the heavy lifting is in `src/pastebin_bisque/cli.py` instead of `main.py`. Additionally, you will find tests that you can run with `tox` and a GitLab CI configuration that ensures all is well. Check out the [docs on how to contribute](https://pastebin-bisque.readthedocs.io/en/latest/contributing.html) for more information. 

# 🐈‍⬛ What's next?

For you: consider running `pip install pastein-bisque` -- on a machine that you're comfortable using to download stuff from Pastebin. 

For me: well, I'm writing this from a rooftop in Amsterdam on my birthday. 🎂 I think I'll log off and celebrate a bit more. 🎊

💖 Be well.
