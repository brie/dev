---
title: 'May 2023 - Three Years at GitLab 🦊💜'
highlight: Three Years at GitLab
description: My third annual look back at the last year on the GitLab Support team. 
excerpt: My third annual look back at the last year on the GitLab Support team. 
slug: three-years-at-gitlab
publishDate: 2023-05-26 20:53:31
tags: ['work','reflections','interview','video','youtube','collaboration','results','efficiency','diversity','inclusion','belonging','values','efficiency','iteration']
author: brie-carranza
authors: Brie Carranza
image: ~/assets/images/posts/pink-flowers.jpg
fullscreen: true
---

> Above is a photograph I took while walking through Coraopolis, PA. Photographing beautiful flowers on long walks is one of my favorite pleasant weather hobbies.

_...in which I present opinions that are my own and not those of my employer._

Hello, world! 

This is the third in my annual series of reflections on the last year at work on the GitLab Support team. (You can take a peek at "[A look back at one year at GitLab](https://brie.dev/year-at-gitlab/)" to see how this tradition began.) I am **really** excited for this year's post! It is a bit different from the other two and it's the end result of a cool collaboration with one of my colleagues: Bruno Freitas.

A few days before my three year anniversary this video was published to [GitLab Unfiltered](https://www.youtube.com/c/GitLabUnfiltered):

  - 🎥 **WATCH** [Bruno Freitas Interviews Brie Carranza | GitLab Support](https://www.youtube.com/watch?v=7hUIyTX_0Ow) (40 minutes)

In this post, I want to highlight some of my favorite bits of our conversation _and_ share a few of the exciting things that happened this year (they didn't all fit in the video). Bruno was a **fantastic** interviewer and I am grateful to him for this idea: thank you, Bruno!

# 💡 Highlights from our  Conversation

## ⚡️ A Few Quick Things

> These are just things that work for me. If they work for you, too: cool! 

  - Have as much organization and structure as you need: no more and no less.
  - Discuss professional development with your manager in a dedicated meeting that you do not **skip**. (Moving it around is OK.)
  - Consider scheduling your 1:1s on the first day of the working week _if possible_.
  - 🚀 On getting things done: publish what you have. Do a good job but don't wait for it to be perfect.
  - Habits are good; ruts are bad. 
    - Cycle out old habits that no longer serve you.
    - Build new habits that help you to achieve your goals.

## 💭 A Few More Words

### 💪 What you know vs what you can figure out

During the interview we talked about a concept I shared it during my [Troubleshooting like Batman talk](https://brie.dev/troubleshooting). I'll share a slightly longer version of it here as well as the emojified version that I mentioned in the interview. 

> What you know is important but it will only get you so far.
> 
> What you can **figure out** will get you home!

One more time but with a few more emoji:

> What you know: 😊
>
> What you can figure out: 😍

I concluded my talk with this sentiment; see the [last slide](https://brie.dev/posts/troubleshooting-like-batman.pdf).

#### 🔥 On Burnout

> We are missing an important conversation if we pretend that we can 💯 **prevent** burnout.

⏩ [Skip straight to this part of the conversation](https://www.youtube.com/watch?v=7hUIyTX_0Ow&t=1537s)!

I think it's helpful to think about the defense in depth (borrowing from cybersecurity) approach here:

  - Prevent burnout
    - Always do your best to prevent it 
    - ...but _know_ what to do if you can't.
  - Detect burnout
  - Respond to burnout
  - Recover from burnout

In this post, I just want to promote this kind of thinking about burnout. I defer to the HBR book I mentioned ([HBR Guide to Beating Burnout](https://store.hbr.org/product/hbr-guide-to-beating-burnout/10406)) and other resources for how to achieve this.

🤗 Be **kind** to yourself. Managing burnout in a kind way is the path to **sustainable excellence**. 

### 🔴 Plan your work; work your plan

A technique to try when you feel "too busy":

**Invest** some time in planning out what you have to do and what you don't have to do.

Write out everything you "have" to do with approximate time estimates. Put everything in order with the most pressing items on top. Draw a line: everything above the lines gets done today. _Get moving._

  - Plan your work, then work your plan.

### 🥅 Public Goal Setting

Setting goals with publicly observable outcomes can be a terrific form of motivation. One of my resolutions for calendar year 2023 was to publish on `brie.dev` at least once per month. I am on track so far: you are reading the fifth of twelve planned posts.  🎉 Head to [brie.dev/blog](https://brie.dev/blog) to check out the first four and a few other things I've written.

In the interview, I mentioned that I was planning on participating in a half-marathon: I am pleased to report that I finished the half-marathon well ahead of my goal time!

#### 🏃‍♀️ Running
In 2022, I committed publicly to running a 5K every month. Injuries and travel prevented me from achieving the goal _but_ I made a lot of running progress last year! I built enough habit and conditioning around it that I am still running regularly and improving on the personal records that I set on my runs last year. It was the **journey** that was important and not the destination.

  - I put [some of the runs from 2022](https://run.brie.dev/) on the incomplete `run.brie.dev`. You can [follow me on Strava](https://www.strava.com/athletes/4306734) to see all my runs.

### ✂️ TIL Snippet Sites

I mentioned [Simon Willison's TIL snippets site](https://til.simonwillison.net/). A TIL snippet site is a great way to capture interesting things that you learned or wanted to record. I assembled [til.brie.dev](https://til.brie.dev/) and update it semi-regularly. I borrowed and modified Simon's work and he was kind enough to [help me more happily use GraphQL and Datasette](https://github.com/simonw/datasette-graphql/issues/92). 

🎒 I am curating a list of [actively maintained or otherwise notable TIL sites](https://gitlab.com/brie/best-of-TIL). Feel free to open an MR or an issue with any suggestions.

### 🧰 Tools

A few of the tools that I use **every day**; some of them were mentioned in the video.

- 📆 Google Calendar
- ✨ Selected Markdown tools
  - [glow](https://github.com/charmbracelet/glow) - Render markdown on the CLI, with pizzazz! 💅🏻
  - [Copy Selection as Markdown](https://chrome.google.com/webstore/detail/copy-as-markdown/nlaionblcaejecbkcillglodmmfhjhfi)
    - This is a game changer! People are excited when I **show** them how it works: please give it a look!
    - TL;DR - It lets you copy parts of a website and paste them as **Markdown**. I use it several times a day.
- [Clockwise](https://www.getclockwise.com/)
  - 🥗 Take a lunch break!
- [espanso](https://espanso.org/) - A privacy-first, cross-platform text expander. 
- ☔️ [Rainy Mood](https://rainymood.com/)

Check out my [tools I added to my toolbox in 2022 🧰](https://brie.dev/2022-tools-added-to-toolbox/) for more of the tools I like. 

# 🎊 Cool Things that Happened This Year

> An extremely short list of some of the coolest and most exciting things that happened during my third year at GitLab!

  - 🎉 I was promoted to **Senior Support Engineer**!
  - 🎤 I delivered my [Troubleshooting like Batman](https://brie.dev/troubleshooting) talk for the first time!
  - 📚 I [participated](https://brie.dev/staff-engineer-path/) in a book club for Tanya Reilly's _amazing_ [The Staff Engineer's Path](https://www.oreilly.com/library/view/the-staff-engineers/9781098118723/).
    - ☕️ I had a coffee chat with an engineering manager at another organization after the book club concluded. As it turned out, he'd been watching the discussion videos that are up on the GitLab Unfiltered YouTube channel. It was extremely cool to chat with him about the book and to hear a firsthand account of the reach that videos on GitLab Unfiltered can have. ✨  
    - 📝 You can read my [notes from the book](https://staffeng.carranza.engineer/BookNotes/) at `staffeng.carranza.engineer`.

# 👁️ READmore

Bruno and I took [public by default](https://handbook.gitlab.com/handbook/values/#public-by-default) to heart with this effort. You can take a look at the [planning issue](https://gitlab.com/bcarranza/bruno-brie/-/issues/1) that we used. It includes the questions we agreed to and our 📋 action items as we put this together. I really enjoyed collaborating with him to plan and deliver on this great idea that he had! 🚀 Thanks again, Bruno.

# 💖 From the Heart 

Back in January 2023, I [recommended](https://brie.dev/2023-january-articles-notes-commands/) savoring moments of happiness  that you  happen upon. I would like to reinforce that notion by reminding the reader that those moments are to be savored because they will pass. Whether "this" is good or bad, remember:

> This too shall pass.

🌌 Savor and seize the day while you can.
