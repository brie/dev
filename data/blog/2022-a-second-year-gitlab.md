---
title: "A second year at GitLab"
description: "How I managed."
slug: second-year-at-gitlab
publishDate: 2022-05-26 21:11:32
tags: ['work','reflections','collaboration','results','efficiency','diversity','inclusion','belonging','values','efficiency','iteration']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/hat-socks-stickers.png
excerpt: "Reflections on a second full year on the GitLab Support team."
fullscreen: false
---

_[You may wish to read "[A look back at one year at GitLab](https://brie.dev/year-at-gitlab/)".]_

Wow! Has it been another year? In this year's post, I want to offer a few of my reflections from the past year on a [few specific subvalues](https://about.gitlab.com/handbook/values/#sub-values-as-substantiators). I also want to zoom in and describe what my day was like today on my second anniversary of joining the GitLab team! A year from now, I think I will be rather glad I recorded a bit more about the day. 

## Reflections

#### Say #thanks...and save #thanks. 

The `#thanks` Slack channel is a nice place to visit to be motivated by how folks are collaborating all the time. In addition to thanking people often, I recommend: **taking a screenshot of compliments and kind words you get from colleagues and customers**. 

Save those screenshots in a folder that is readily accessible. Those screenshots are great:

  - for lifting you up on days when you need it!
  - material for performance reviews and promotion documents!
  - reminders that people do notice and recognize the hard work you do!

#### Connect

The emphasis on [getting to know one another](https://about.gitlab.com/handbook/values/#get-to-know-each-other) makes it really easy to connect with folks across the organization. I have had so many warm, fascinating and fun coffee chats with people from across the world. While I am physically alone in my office, I have found coffee chats a great way to feel very connected to the team and to learn a little bit about what life is like for people around the world.  

#### Embrace boring solutions

I have learned to love the embrace of [boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions). 

Boring solutions are a key to iteration, in my mind. Building things is super exciting and it is easy to get carried away with brainstorming and wanting to do all the things. Embracing boring solutions and [the smallest possible version](https://about.gitlab.com/handbook/values/#move-fast-by-shipping-the-minimal-viable-change) of the thing have been 🔑 key to helping me to better understand and adopt [iteration](https://about.gitlab.com/handbook/values/#iteration).   

  - I would rather get `v0` out than be toiling away forever at exciting features that really belong in a wish list. 
  - This applies to processes as well as to development: boring solutions are more likely to be simple, effective and reliable. 

#### Persist. Perservere.

The note on [tenacity](https://about.gitlab.com/handbook/values/#tenacity) and persistence of purpose has always called to me. 

  - Life is tough. You will be knocked down. What counts is how you handle it.

The general sentiment behind [fall down seven times, get up eight](https://www.presentationzen.com/presentationzen/2011/03/fall-down-seven-times-get-up-eight-the-power-of-japanese-resilience.html) feels a bit pat and like a cliché. Perhaps that's simply because it continues to resonate.    

## Less and More

Compared to my role prior to joining GitLab, I do:

  - **LESS** hardware
  - **LESS** `./configure && make && sudo make install` 
    - I was about 50/50 doing this inside a container or straight on a host! 😂
  - **MORE** Ruby
    - Two years in: I am pretty happy with the bits of Ruby I have written.   
  - **LESS** Python
  - **THE SAME** Golang
  - **LESS** Singularity, **MORE** Docker
  - **LESS** Apache, **MORE** nginx
  - **MORE** Kubernetes


## The Day

> Here's an overview of what I did today!

I had a really lovely day, bookended with engaging [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) with some awesome people. 

  - **0900** Coffee Chat / Get Together 
  - **1130** AMA on becoming a Staff Support Engineer
  - **1245** Cuddle with my cat
  - **1400** Benefits webinar
  - **1500** Pairing
  - **1600** Meeting with my mentor
  - **1700** Coffee Chat
  - **1900** Coffee Chat
  
In between those synchronous meetings, I focused on tickets involving:

  - troubleshooting GitLab CI behavior
  - debugging a REST API response
  - PostgreSQL segmentation faults
  - repository size reporting 
  - diagnosing Sidekiq behavior


That's a fairly high number of synchronous meetings. In that sense, it was not a typical day but it made my two year anniversary feel extra special to get to spend time with so many of the really wonderful folks I get to work with every single day.  


---

I wrote all of the above before re-reading "[A look back at one year at GitLab](https://brie.dev/year-at-gitlab/)". 

After reviewing that post for the first time in quite a while: it all holds true.  I feel compelled to close this post precisely as I did the last:

> Above all, I am so thankful for the goodness and kindness of the people I have met at GitLab. There are far far too many names to enumerate but please know that I appreciate every single Slack mention, #thanks post, issue comment, MR, pairing session and [coffee chat](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats).

--

Brie 

 
