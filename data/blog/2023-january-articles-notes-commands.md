---
title: "January 2023 - Awesome Articles and Cool Commands"
description: "A curated list of links accompanied by notes about the things I found interesting this month."
excerpt: "A curated list of links accompanied by notes about the things I found interesting this month."
slug: 2023-january-articles-notes-commands
publishDate: 2023-01-31 23:59:21
tags: ['git','yaml','troubleshooting','tools','lnav','log analysis','datasette','certificate authority']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/joyful-january.jpg
fullscreen: true
created: 2023-01-25T21:22:34-05:00
modified: 2023-01-31T23:42:23-05:00
---

Hello, world! 

I'm trying something new this year! In this post, I have a ✨ **curated** ✨ list of interesting articles, tools and neat links that I found particularly impactful or helpful this month. If you are a frequent reader of [HN](https://news.ycombinator.com), you may recognize some of these items. This is not a "best of HN - January 2023" review. It's closer to "things that were on my mind - January 2023". 

> OK, but who are you?

Great question! 👋 I'm [Brie Carranza](https://hello.brie.dev/) and I welcome you to [learn a little bit more about me](https://brie.dev/about/). Taking a quick look at [my blog](https://brie.dev) might give you some insight into what I'm interested in and whether or not this series may be of interest to you. Let's chat!

  - [@whoamibrie](https://twitter.com/whoamibrie)
  - [@brie@infosec.exchange](https://infosec.exchange/@brie)

Enjoy and be well!

--
Brie

## 🔖 Good Reads

> Take a few moments, grab a notebook and read through these articles! 

- [Conversation Skills Essentials](https://tynan.com/letstalk/)
  - I reflect often on this article. There are so many gems but I'm particularly struck by the notes on positive disagreement. Once you've read the article: consider what it _feels_ like when someone disagrees positively with you. 
- [Ghost Ships](https://logicmag.io/pivot/ghost-ships/): What happens when ships become data? 
- [A high level guide to USB-C](https://usbc.wtf/a-high-level-guide-to-usb-c)
  - In addition to being a well-written moderately deep dive into USB-C, they get so many points for an awesome domain name like `ubsc.wtf`. It's a site dedicated towards reducing the confusion that is USB-C.
- [The Window Trick of Las Vegas Hotels](https://www.schedium.net/2023/01/the-window-trick-of-las-vegas-hotels.html)
  - A fascinating read on our conflicting needs for both order and variety and a really interesting observation about the _impact_ of how windows are grouped in Las Vegas hotels. I can't _not_ notice it now!  

### Learning a new codebase - for non-developers

I saw [Grokking Big Unfamiliar Codebases](https://www.jeremyong.com/game%20engines/2023/01/25/grokking-big-unfamiliar-codebases/) on HN and got really curious about what else has been written on this topic. I spend far more time reading other codebases and using that knowledge to solve problems than I do writing my own code. In other words: I am not a developer. However, it's still _extremely_ helpful for folks in roles like mine to be able to get a handle on a new codebase. When troubleshooting, there are a few options:

  - Test how the thing works, observe the results and construct a new test
  - Read how the thing _should_ work: testing may no longer be necessary; if it is necessary, you can build much more well-informed tests

The latter is the more elegant option, in my opinion. Without a reasonable understanding of the codebase, it might be very difficult to pull off the latter option. The first option is more of a blunt instrument but it will get the job done -- in time. 

If the description above resonates with you, I can highly recommend these articles for non-developers who would like to start to get their arms and brain wrapped around a codebase:

  - [new codebase, who dis? (How to Join a Team and Learn a Codebase)](https://www.samueltaylor.org/articles/how-to-learn-a-codebase.html)
  - [Learn a new codebase](https://xdg.me/learn-a-new-codebase/)

A few notes:

  - This won't make you an expert on the codebase overnight -- or maybe ever. 
    - The goal here is not to become an expert on the codebase. 
    - The goal here is to get enough of a handle on the big picture that you know where to look. The point is a general situational awareness that provides helpful context for later investigations. 
  - This exercise might raise a lot of questions. Good! Record them and track your progress on answering them over time. 

### Fun with YAML

- [The yaml document from hell](https://ruudvanasseldonk.com/2023/01/11/the-yaml-document-from-hell)
- [No YAML](https://noyaml.com/)

The Norway thing comes up on [noyaml.com](https://noyaml.com). IYKYK but if you haven't read about the trouble with abbreviating Norway and YAML, it's a wild ride well worth a read. A good entry point is:

  - [YAML: The Norway Problem](https://www.bram.us/2022/01/11/yaml-the-norway-problem/)

#### `---`

One of my favorite YAML tricks is for troubleshooting GitLab CI and uses the power of `---`. Let's say you want to quickly comment out a bunch of jobs and other stuff in your `.gitlab-ci.yml` file. Yes, it's `git` so you can just delete, commit and revert. Considering [the beginner mindset](https://zenhabits.net/beginner/): that kind of `git` wizardry might take longer than it's worth. Here's what I like to do instead:

  1. Put the content that I want to run at the top of the file
  1. Add a line that only contains `---`
  1. Plop everything that I don't want to run below that line

Only the jobs that I define _above_ the `---` line will get added to the pipeline! 

From there, I can iterate on the job(s) above the line until I get it right. I can simply remove the `---` line and I get my whole pipeline back! 

This is for troubleshooting or experimenting in test branches where the somewhat messy `git` history that this creates does not matter. 

  - No need to add `.` before each and every job name to hide them. 
  - No need to type `#` and the Enter key a billion times (👀👀👀).
    - If this sounds like you, it might be worth spending a little bit of time with `sed`. I promise it's worth it. 

**What's going on here? Why does this work?** 

Well, YAML understands you saying `---` to mean that you want to create a new YAML document. The stuff below the line "isn't in `.gitlab-ci.yml`". 

## ⚒️ Cool Tools

  - [espanso](https://espanso.org/) -  A Privacy-first, Cross-platform Text Expander
    - This is a tool I wish I had picked up sooner. I do not use it all of the time but it's invaluable when I do wield it. I haven't documented my Espanso config anywhere as of this writing.  
  - [libSQL](https://github.com/libsql) - libSQL is a fork of SQLite that is both Open Source, and Open Contributions.
  - lnav - [The Logfile Navigator](https://lnav.org/)

Whoa! I love `lnav` because it is a fantastic general purpose log analysis tool! I've used it to parse logs from a whole bunch of different services and it does a really good job with content from the likes of . I have only started to explore the feature set but it's wonderful so far. You'll probably here more from me on `lnav`. 

  - [XCA](https://hohnstaedt.de/xca/)

Let's face it: when you are troubleshooting a problem, the involvement of a self-signed certificate pumps things up a level. It's not impossible, it's just a little bit harder. For testing and troubleshooting purposes, it can be very helpful to be able to:

  - quickly generate a self-signed certificate
  - act as your own CA (certificate authority)

It's possible to do all of these things without being a TLS wizard! Enter [XCA - X - Certificate and Key management](https://hohnstaedt.de/xca/)! XCA stores all of it's cryptographic data in a database and SQLite is supported! 

While you're at it, [How to destroy a certificate authority in one month](https://cohost.org/arborelia/post/868933-how-to-destroy-a-cer) is a great companion read! 

## Fun Commands

### My favorite new-ish git feature! 

Seriously: I'm so pumped for this one! Let's set the stage:

  - You cloned your repo.
  - You used `git checkout -b dev/cute-cats` or similar and then committed your awesome work. 
  - You run `git push` and `git` yells at you:


```
fatal: The current branch dev/2023-01-january has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin dev/2023-01-january
```

_OK, so you know what I want: just do it!_

No more! That message now lets us know a bit more about my new favorite git feature:

```
To have this happen automatically for branches without a tracking
upstream, see 'push.autoSetupRemote' in 'git help config'.
```

YES PLEASE! For my personal workflow: I can't think of a situation when I would _not_ want this to happen automatically! For a reasonable description and links to the relevant commits, check [this SO answer](https://stackoverflow.com/questions/6089294/why-do-i-need-to-do-set-upstream-all-the-time/72401899#72401899). 

> "new-ish"? Do I have this feature?

It's not super new but this is my first opportunity to write about it and I'm _still_ excited that it exists! 

This joy comes to us in Git 2.37.0 so ask `git --version` if you have it. 

### Datasette Favorites 

I really started to appreciate [datasette](https://datasette.io/) towards the end of last year. I used it to build [this heat map](https://412.brie.run) of Pittsburgh's "city steps". I have enough `datasette` instances open often enough that this has saved me lots of grumbling:

```
datasette serve -p 0
```

The `-p 0` tells `datasette` to choose an _available_ port instead of constantly trying to use `:8001`. Banish this error message from your life with `datasette serve -p 0`:

```
ERROR:    [Errno 48] error while attempting to bind on address ('127.0.0.1', 8001): address already in use
```

As a bonus: adding `-p 0` isn't all that much extra typing! 

### QUICK: put a password in my clipboard! 

As I do more personal computing on 🍏 than 🐧 , I found myself needing to port my `apg` workflow for generating random passwords on the fly. I am presently using `pwgen` for this purpose. I _usually_ use this when I want a reasonable password but don't really care about remembering it (for whatever reason). Something like this is reasonable:

```
pwgen -1 -N1 -y -s 20 | pbcopy
```

I highly recommend taking a look at `pass` which can work nicely with `git-crypt`. _insert chef's kiss emoji_

  - [pass](https://www.passwordstore.org/) - the standard unix password manager
  - [git-crypt](https://github.com/AGWA/git-crypt) - Transparent file encryption in git

## 💡That's Neat!

These days [@depthsofwiki](https://twitter.com/depthsofwiki/) is one of my favorite novelty accounts on the bird app. The [tweet about](https://twitter.com/depthsofwiki/status/1617952116294963200) a picture of a flower getting 90 million hits a day was fascinating and I had to know more: you can read the [public investigation](https://phabricator.wikimedia.org/T273741). 

We can make some good guesses about what the developers of the app were attempting to do by loading that image. The consequences of such a change are clear and I can even totally understand using an image hosted by The Wikimedia Foundation. (We probably all take the reliability of Wikipedia and other projects over the **decades** for granted. It's always there: thank you, team!) 

Here's your **homework** or **food for thought**:

  - How would you solve this problem differently?

## From the Heart

If you find yourself experiencing a moment of happiness, savor it. 

```
FIN.
```
