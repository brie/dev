---
title: "October 2023 - Packaging a Python Program for PyPI: Tools, Tips + GitLab CI Config"
highlight: "A collection of things I learned while publishing a Python package to PyPI in 2023."
description: "A collection of things I learned while publishing a Python package to PyPI in 2023."
excerpt: "A collection of things I learned while publishing a Python package to PyPI in 2023."
slug: 2023-python-packaging
publishDate: 2023-10-28 20:19:31
tags: ['python','programming','pypi','twine','package']
author: brie-carranza
authors: "Brie Carranza"
image: ~/assets/images/posts/heart-cat-learning.png
fullscreen: true
---

TL;DR | The tools that I recommend are [bump2version](https://github.com/c4urself/bump2version) and [cookiecutter](https://github.com/cookiecutter/cookiecutter) (with the [cookiecutter-pylibrary](https://github.com/ionelmc/cookiecutter-pylibrary) template in particular) and the tips are "use [Test PyPI](https://test.pypi.org)" and "🤷‍♀️ maybe there's no need to sign your PyPI package". 

---

🌊 Hello, world! 

Last month, I [announced](https://brie.dev/2023-september-pastebin-bisque-v1/) the 🚀 release of `v1.0` of `pastebin-bisque`, a Python program I've been working on since 2020. The `v1.0` release included publishing `pastebin-bisque` on PyPI. I wanted to take a moment to write about some of the most helpful tools and notes from my experience.

This is intended to be a quick blog post but here's an even quicker outline:

- 🥠 The power of `cookiecutter`
  - AKA how to not write annoying or boring code and skip straight to the stuff you wanted to write in the first place
- 🚀 Umm, `bump2version` is awesome
  - A super useful tool for managing the process of releasing a new version of a package
- 🧪 Use [Test PyPI](https://test.pypi.org/) (`test.pypi.org`)
  - To make sure your package looks right before going live
- 🖋 On package signing 
  - ...and why I declined
- 💚 Plus: the full `.gitlab-ci.yml` that I use to
  - Run tests against Python 3.8-3.11 with `tox`
  - Build the package
  - Upload the package to GitLab Container Registry, Test PyPI and PyPI
    - Authenticate to (Test) PyPI with a token
  - Install and run the package that is published to the registries above

## 🖼 The Big Picture

A few words on the big picture, for context:

- In early 2020, I started [pastebin-bisque](https://github.com/bbbbbrie/pastebin-bisque) with [this commit](https://github.com/bbbbbrie/pastebin-bisque/commit/752d1cc7e3c2c273ebced034334fc487d59690f4) on [February 26](https://en.wikipedia.org/wiki/Portal:Current_events/2020_February_26).
  -  To use it, you did `pip install -r requirements.txt` and then `python main.py` after cloning the `git` repo.
- In 2022, `pastebin-bisque` started getting some attention and I wanted to make the program easier to **use** and easier to **develop** and **contribute** to.
  - Plus, I always wanted to make a proper Python package on PyPI.  (I didn't want to do it _just_ to do it; I wanted to have a good reason and I finally had one.)
- In September 2023, I refactored the code so that it could be installed via `pip install pastebin-bisque` and executed with `pastebin-bisque`.
  - A significant part of that effort was moving the [contents of `main.py`](https://sourcegraph.com/gitlab.com/brie/pastebin-bisque@1b7ec93ba76724b7632f02ec7ee9d407883662ef/-/blob/main.py) to [a new home in `src/pastebin_bisque/cli.py`](https://sourcegraph.com/gitlab.com/brie/pastebin-bisque/-/blob/src/pastebin_bisque/cli.py).

This post is a quick(ish) list of the stuff I learned and the tools that were most helpful for me during this process. I'm excluding the obvious stuff that is better explained elsewhere, like `git` and `tox`. I assume that the reader has some familiarity with writing Python.

## 🥠 The power of `cookiecutter`

I used `cookiecutter` to save ⏱ time. Specifically, I wanted to get to the exciting part and I didn't want to write (boring) skeleton code.

So, `cookiecutter` [is](https://github.com/cookiecutter/cookiecutter): 

> A cross-platform command-line utility that creates projects from cookiecutters (project templates), e.g. Python package projects, C projects.

After inspecting several options, the specific `cookiecutter` template that I used is [cookiecutter-pylibrary](https://github.com/ionelmc/cookiecutter-pylibrary) and it [took care](https://github.com/ionelmc/cookiecutter-pylibrary#id3) a bunch of stuff including:

- `tox` for testing `pastebin-bisque` against multiple versions of Python
- Documentation with Sphinx -- ready for use with [ReadTheDocs](https://readthedocs.org)
- Support for `bump2version` (keep reading...)

The easiest way to demonstrate this might be to take a look at the output of `tree` after generating the project with `cookiecutter`. 

```shell
cookiecutter gh:ionelmc/cookiecutter-pylibrary
```

The `cookiecutter` command prompts one to answer [quite a few questions](https://github.com/ionelmc/cookiecutter-pylibrary#id5). After accepting the defaults, the directory structure looks like this: 

```
# tree python-cutestcat 
python-cutestcat
├── AUTHORS.rst
├── CHANGELOG.rst
├── CONTRIBUTING.rst
├── LICENSE
├── MANIFEST.in
├── README.rst
├── ci
│   ├── bootstrap.py
│   ├── requirements.txt
│   └── templates
├── docs
│   ├── authors.rst
│   ├── changelog.rst
│   ├── conf.py
│   ├── contributing.rst
│   ├── index.rst
│   ├── installation.rst
│   ├── readme.rst
│   ├── reference
│   │   ├── cutestcat.rst
│   │   └── index.rst
│   ├── requirements.txt
│   ├── spelling_wordlist.txt
│   └── usage.rst
├── pyproject.toml
├── pytest.ini
├── setup.py
├── src
│   └── cutestcat
│       ├── __init__.py
│       ├── __main__.py
│       └── cli.py
├── tests
│   └── test_cutestcat.py
└── tox.ini

8 directories, 28 files
```

😎 Cool! The emphasis was on editing `src/cutestcat/cli.py` to start to add my code.  Once the `cookiecutter` invocation was complete, I had a `pastebin-bisque` executable. I started bringing the imports and functions from `main.py` into `src/pastebin_bisque/cli.py` until the functionality of the original program was restored.

Effectively, this is the loop I used while actively developing:

```
while true ; do 
    vim src/cutestcat/cli.py ; 
    rm -f $(which cutestcat) && python setup.py install && cutestcat ; 
done
```

The `rm -f $(which cutestcat) && python setup.py install && cutestcat` loop removes the old executable, creates a new one based on my changes and runs it. I stayed in that loop until things worked the way I wanted or until I needed to take a break.

## 🚀 Umm, `bump2version` is awesome

The `cookiecutter` prompts asked about `tbump` or `bump2version`. I'd been interested in `bump2version` already anyway so I chose it and it's really nice. I would definitely recommend further adjusting `.bumpversion.cfg` to customize things the way you'd like.

You'd then use one of these commands depending on what kind of version you'd like to release:

- `bump2version major`
- `bump2version minor`
- `bump2version patch`

If you followed along in the earlier section and want to test the release process, you might find this loop helpful:

```shell
pre-commit run --all-files &&  \
    git commit -am"✨ Use dependencies from PyPi" && \
    bump2version patch && git push --tags
```

The `.bumpversion.cfg` file contains the `current_version` and has information about where to replace the current version with the new version in your project. You'll see `setup.py` and `docs/conf.py` and files like that in `.bumpversion.cfg`. 

🚀 Releasing a new major version becomes: 

1. `bump2version major && git push --tags`
1. Go watch the pipeline!
1. ▶️  Press **Play** to publish the package

The "🚀 deploy to 🎊 PyPi" [job](https://sourcegraph.com/gitlab.com/brie/pastebin-bisque@505ff36d81dc0be38b8f836d24c5be00288ff012/-/blob/.gitlab-ci.yml?L113-121) that publishes the package to [PyPI](https://pypi.org) is a [manual job](https://docs.gitlab.com/ee/ci/jobs/job_control.html#create-a-job-that-must-be-run-manually) so it _requires_ me to push **Play** when the earlier steps are complete.  

## 🧪 Use TestPyPI (`test.pypi.org)`)

Since this was the first Python package I'd published, this was the first time I had use for something like TestPyPI. Given the permanence of releases on PyPI, it's _very_ nice to have a testing environment that mimics the real thing pretty well. You can totally see [my tests](https://test.pypi.org/project/pastebin-bisque/). 

The docs on 📚 [Using TestPyPI](https://packaging.python.org/en/latest/guides/using-testpypi/) are pretty great but I'll say a little more.

### ✍️ On PyPI and TestPyPI

You can use our [friend](https://pypi.org/project/twine/) `twine` to upload to TestPyPI as I do in the [🚀 deploy to 🧪  TestPyPi](https://sourcegraph.com/gitlab.com/brie/pastebin-bisque/-/blob/.gitlab-ci.yml?L101-110) CI job (after `python -m build` succeeds):

```shell
TWINE_USERNAME=__token__ TWINE_REPOSITORY_URL=https://test.pypi.org/legacy/ \
    twine upload --non-interactive --comment "🦄 hello world" \
    --skip-existing --password $TESTPYPI_TOKEN  dist/*
```

- `TWINE_USERNAME`: You'll want to set `TWINE_USERNAME` to `__token__` if you are [using an API token to authenticate](https://pypi.org/help/#apitoken).
- `TESTPYPI_TOKEN`: You'll also need to set `TESTPYPI_TOKEN` to the value of the API token.

A note about using TestPyPI: do not expect that every package on PyPI is also on TestPyPI. In my [GitLab CI config](https://gitlab.com/brie/pastebin-bisque/-/blob/main/.gitlab-ci.yml?ref_type=heads), I do this to install my package from TestPyPI while getting the dependencies from PyPI:

```shell
pip install -i https://test.pypi.org/simple/ pastebin-bisque \
    --extra-index-url https://pypi.org/simple
```

Another tip: don't trust the namespaces to have the same ownership between PyPI and TestPyPI. In other words: the person who authored the package at `test.pypi.org/project/whatever` may not be the person who authored the package at `pypi.org/project/whatever`.

This doesn't mean much but as of this writing:

- 🧪 TestPyPI: 
  - 180,202 projects 
  - 964,359 releases 
  - 1,993,081 files 
  - 171,379 users
- 🚀 PyPI: 
  - 487,007 projects 
  - 4,962,866 releases 
  - 9,315,735 files 
  - 748,166 users

...it does mean that there are a few hundred _thousand_ projects that are not on TestPyPI. What's the angle? If I wanted to target Python developers, I would look for projects that are not on Test PyPI. You want something that is used often enough but isn't super high profile. A high profile package is likely to already be on TestPyPI or for its sudden appearance on TestPyPI to be noticeable. This is a more subtle, less scattershot approach to the kinds of things written up in articles like [10 malicious PyPI packages found stealing developer's credentials](https://www.bleepingcomputer.com/news/security/10-malicious-pypi-packages-found-stealing-developers-credentials/). 🤷‍♀️ It probably would not be worth it but one might get "lucky". 😉

## 🖋 On package signing and why I declined

When I looked into signing the package I was building, I ultimately decided against proceeding. While I don't hold all of the views in the linked articles, this summarizes my thinking and is followed by a more extensive reading list: 

> [TL;DR: A large number of PGP signatures on PyPI can’t be correlated to any well-known PGP key and, of the signatures that can be correlated, many are generated from weak keys or malformed certificates.](https://blog.yossarian.net/2023/05/21/PGP-signatures-on-PyPI-worse-than-useless)

source: [PGP signatures on PyPI: worse than useless](https://blog.yossarian.net/2023/05/21/PGP-signatures-on-PyPI-worse-than-useless)

### 📚 Reading List

- [PyPI and gpg signed packages](https://kushaldas.in/posts/pypi-and-gpg-signed-packages.html)
- [Package signing in PIP - It works, in a roundabout sort of way](https://prahladyeri.github.io/blog/2019/06/package-signing-in-pip-it-works-in-a-roundabout-sort-of-way.html)
- [PGP signatures on PyPI: worse than useless](https://blog.yossarian.net/2023/05/21/PGP-signatures-on-PyPI-worse-than-useless)
  - Source: [woodruffw/pypi-pgp-statistics](https://github.com/woodruffw/pypi-pgp-statistics)
- [GPG signing - how does that really work with PyPI?](https://github.com/pypa/twine/issues/157)
- [Remove GPG references from publishing tutorial](https://github.com/pypa/packaging.python.org/pull/466)  

I wound up declining to sign the `pastebin-bisque` package. The time required would not be worth the benefit at this point in time. 

## 🛸 Extra: Full `.gitlab-ci.yml` for building and deploying Python package to GitLab Package Registry, TestPyPI and PyPI

Want to see the `.gitlab-ci.yml` that I am using for building, testing and releasing the `pastebin-bisque` package to GitLab's **Package Registry**, TestPyPI and to PyPI?

Here's what these looked like in `v1.0.0`:

- 🦊 See the [.gitlab-ci.yml](https://gitlab.com/brie/pastebin-bisque/-/blob/v1.0.0/.gitlab-ci.yml?ref_type=tags) for `v1` of `pastebin-bisque` on GitLab.
- 📊 See the [.gitlab-ci.yml](https://sourcegraph.com/gitlab.com/brie/pastebin-bisque@505ff36d81dc0be38b8f836d24c5be00288ff012/-/blob/.gitlab-ci.yml) for `v1` of `pastebin-bisque` on Sourcegraph.

# 🐈‍⬛ What’s next?

As the [10th Hacktoberfest](https://hacktoberfest.com/) comes to an end, I'll be releasing `v1.0.1` of `pastebin-bisque` and I'm super excited about the new contributors whose work will be included in that release: 😻 thank **you**!